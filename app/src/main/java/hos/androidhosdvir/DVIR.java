package hos.androidhosdvir;

import android.annotation.TargetApi;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import hos.androidhosdvir.db.DBHelper;
import hos.androidhosdvir.dvir.dvir_general;
import hos.androidhosdvir.dvir.dvir_sign;
import hos.androidhosdvir.settings.Settings;
import hos.androidhosdvir.util.Util;
import hos.androidhosdvir.util.UtilDate;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DVIR extends Fragment{
    public static View rootView;
    private ImageView fab, logout;
    public static int trailer_defect_ind[];
    public static String response;
    public static LinearLayout dvir_layout;
    public static Context app_context, activity_context;
    public static TextView log_title;
    public JSONObject row_dvir;
    public static JSONArray dvirData = new JSONArray();
    public static ArrayList<Defects> vehicleDefects, trailerDefects;
    public static Boolean isVehicleEdited = false, isTrailerEdited = false;
    public static ConnectivityManager connectivityManager;
    public static ImageView date_pick, settings;
    public static JSONObject DVIRJSONObj = new JSONObject();
    public static DatePickerDialog datePickerDialog;
    // public static ProgressDialog pdia;
    public static ProgressBar progressBar;
    // Temp variables to store values till report is saved
    public static String temp_time = "", temp_location = "", temp_odometer = "", temp_carrier = "", temp_vehicle_no = "", temp_trailer_no = "", temp_miles = "", temp_hours = "", temp_mechanic_notes = "";
    public static int temp_defects_resolved = 0, temp_defects_unresolved = 0, temp_driver_signed = 0, temp_mechanic_signed = 0, temp_inspection_type = 2;
    public static JSONArray temp_vehicle_defects = new JSONArray(), temp_trailer_defects = new JSONArray();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.dvir, container, false);
        trailer_defect_ind = new int[]{1, 3, 4, 5, 8, 9, 10, 11, 17, 18, 20, 21, 22};
        dvir_layout = (LinearLayout) rootView.findViewById(R.id.dvir_layout);
        date_pick = (ImageView) rootView.findViewById(R.id.date_pick);
        settings = (ImageView) rootView.findViewById(R.id.settings);
        app_context = getContext();
        HomeActivity.cur_fragment = "DVIR";
        vehicleDefects = new ArrayList<Defects>();
        trailerDefects = new ArrayList<Defects>();
        connectivityManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        activity_context = getActivity();
        logout = (ImageView) rootView.findViewById(R.id.logout);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DBHelper.logout(DBHelper.getLoggedInUserId());
                Intent i = new Intent(getActivity(), Login.class);
                startActivity(i);
            }
        });

        log_title = (TextView) rootView.findViewById(R.id.log_title);
        if (Login.cur_date.equals(Util.getTodayDate())) {
            log_title.setText("Log for today");
        } else {
            log_title.setText("Log for " + Login.cur_date);
        }

        dvirData = DBHelper.getDvirData(DBHelper.getLoggedInUserId(), Login.cur_date);
        displayDVIR();
        // retain this fragment
        setRetainInstance(true);

        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.main_content, new Settings(), "settings");
                transaction.addToBackStack(null);
                transaction.commitAllowingStateLoss();
            }
        });

        date_pick.setOnClickListener(new View.OnClickListener() {
            String date = Util.getTodayDate();
            String[] date_split = date.split("/");
            int day = Integer.parseInt(date_split[1]); // 21
            int month = Integer.parseInt(date_split[0]); // 1
            int year = Integer.parseInt(date_split[2]); // 2018

            @TargetApi(Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {
                // Log.d("Current date", day+" "+(month+1)+" "+year);
                datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int selectedYear, int selectedMonth, int selectedDay) {
                        Date date = new Date();
                        Calendar cal = Calendar.getInstance();
                        cal.set(selectedYear, selectedMonth, selectedDay);
                        date.setTime(cal.getTime().getTime());

                        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
                        String dt = dateFormat.format(date);
                        Login.cur_date = dt;

                        Fragment frg = getFragmentManager().findFragmentByTag("DVIR");
                        final FragmentTransaction ft = getFragmentManager().beginTransaction();
                        ft.detach(frg);
                        ft.attach(frg);
                        ft.commit();
                    }
                }, day, month - 1, year);

                Date curdate = new Date();
                DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
                try {
                    curdate = df.parse(date);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                Calendar cal = new GregorianCalendar();
                cal.setTime(curdate);
                cal.add(Calendar.DAY_OF_MONTH, -90);
                curdate.setTime(cal.getTime().getTime());

                Date todayInUserTimezone = UtilDate.getCurrrentLocalDateInDateObject(DBHelper.getDefaultHomeTimezoneinGTM(DBHelper.getLoggedInUserId()));
                datePickerDialog.getDatePicker().setMaxDate(todayInUserTimezone.getTime());

                //datePickerDialog.getDatePicker().setMaxDate(new Date().getTime());
                datePickerDialog.getDatePicker().setMinDate(curdate.getTime());
                // Set Max date and Min date for add date
                datePickerDialog.updateDate(year, month - 1, day);
                datePickerDialog.show();
            }
        });

        // Add new DVIR
        fab = (ImageView) rootView.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeActivity.isEdit = false;
                // Time
                temp_time = "";

                // Location
                temp_location = "";

                // Odometer
                temp_odometer = "";

                // Carrier
                temp_carrier = "";

                // Vehicle number
                temp_vehicle_no = "";

                // Trailer number
                temp_trailer_no = "";

                // Miles
                temp_miles = "";

                // Hours
                temp_hours = "";

                // Inspection Type
                temp_inspection_type = 2;

                // Defects resolved
                temp_defects_resolved = 0;

                // Defects unresolved
                temp_defects_unresolved = 0;

                // Mechanic notes
                temp_mechanic_notes = "";

                temp_driver_signed = 0;

                temp_mechanic_signed = 0;

                temp_vehicle_defects = new JSONArray();

                temp_trailer_defects = new JSONArray();

                HomeActivity.mechanicSign = "";

                HomeActivity.driverSign = "";

                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.main_content, new dvir_general(), "dvir_general");
                transaction.addToBackStack(null);
                transaction.commitAllowingStateLoss();
            }
        });

        return rootView;
    }

    @Override
    public void onDetach() {
        super.onDetach();

        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);

        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public void displayDVIR(){
        try {
            if(dvirData != null && dvirData.length() > 0) {
                dvir_layout.removeAllViews();

                for (int i = 0; i < dvirData.length(); i++) {
                    LinearLayout layout = new LinearLayout(getContext());
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    params.setMargins(10, 10, 10, 10);
                    layout.setLayoutParams(params);
                    layout.setOrientation(LinearLayout.VERTICAL);

                    GradientDrawable border = new GradientDrawable();
                    border.setColor(0xFFFFFFFF); //white background
                    border.setStroke(3, 0xFF000000); //black border with full opacity
                    layout.setBackground(border);

                    row_dvir = new JSONObject();
                    row_dvir = dvirData.getJSONObject(i);

                    params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 80);
                    final TextView dvir_no = new TextView(getContext());
                    dvir_no.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 80));
                    dvir_no.setText("Report #"+(i+1));
                    dvir_no.setTypeface(Typeface.SERIF, Typeface.BOLD);
                    dvir_no.setTextColor(Color.BLACK);
                    dvir_no.setPadding(10, 5, 0, 2);
                    dvir_no.setGravity(Gravity.CENTER);
                    dvir_no.setBackgroundResource(R.drawable.btn_layout);
                    layout.addView(dvir_no);

                    final TextView dvir_text = new TextView(getContext());
                    dvir_text.setLayoutParams(params);
                    dvir_text.setText(row_dvir.getString("dvirid"));
                    dvir_text.setVisibility(View.GONE);
                    layout.addView(dvir_text);

                    LinearLayout time_layout = new LinearLayout(getContext());
                    time_layout.setLayoutParams(params);
                    time_layout.setOrientation(LinearLayout.HORIZONTAL);
                    time_layout.setWeightSum(1);

                    ImageView time_clock = new ImageView(getContext());
                    time_clock.setLayoutParams(new LinearLayout.LayoutParams(100, LinearLayout.LayoutParams.MATCH_PARENT));
                    try {
                        //time_clock.setForegroundGravity(Gravity.CENTER);
                    }catch(Exception e){
                        e.printStackTrace();
                    }
                    time_clock.setPadding(10, 5, 0, 2);
                    time_clock.setImageResource(R.drawable.time_avail);
                    time_layout.addView(time_clock);

                    TextView time = new TextView(getContext());
                    time.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT, 0.8f));
                    time.setText(row_dvir.get("time").toString());
                    time.setTypeface(Typeface.SERIF);
                    time.setTextColor(Color.DKGRAY);
                    time.setPadding(10, 5, 0, 2);
                    time.setGravity(Gravity.CENTER);
                    time.setTextAppearance(getContext(), android.R.style.TextAppearance_DeviceDefault_Medium);
                    time_layout.addView(time);
                    layout.addView(time_layout);

                    final ImageView edit_dvir = new ImageView(getContext());
                    edit_dvir.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT, 0.1f));
                    try {
                        if(android.os.Build.VERSION.SDK_INT >= 21) {
                            edit_dvir.setImageDrawable(getResources().getDrawable(R.drawable.edit_icon, getContext().getTheme()));
                        } else {
                            edit_dvir.setImageDrawable(getResources().getDrawable(R.drawable.edit_icon));
                        }
                    } catch (Exception e){
                        e.printStackTrace();
                    }
                    edit_dvir.setPadding(10,10,10,10);
                    try{
                        //edit_dvir.setForegroundGravity(Gravity.RIGHT);
                    }catch(Exception e){
                        e.printStackTrace();
                    }
                    time_layout.addView(edit_dvir);

                    final ImageView delete_dvir = new ImageView(getContext());
                    delete_dvir.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT, 0.1f));
                    try {
                        if(android.os.Build.VERSION.SDK_INT >= 21) {
                            delete_dvir.setImageDrawable(getResources().getDrawable(R.drawable.delete_icon, getContext().getTheme()));
                        } else {
                            delete_dvir.setImageDrawable(getResources().getDrawable(R.drawable.delete_icon));
                        }
                    }catch(Exception e){
                        e.printStackTrace();
                    }
                    delete_dvir.setPadding(10,10,10,10);
                    try{
                        //delete_dvir.setForegroundGravity(Gravity.RIGHT);
                    }catch(Exception e){
                        e.printStackTrace();
                    }
                    time_layout.addView(delete_dvir);

                    LinearLayout location_layout = new LinearLayout(getContext());
                    location_layout.setLayoutParams(params);
                    location_layout.setOrientation(LinearLayout.HORIZONTAL);
                    location_layout.setWeightSum(1);

                    ImageView location_icon = new ImageView(getContext());
                    location_icon.setLayoutParams(new LinearLayout.LayoutParams(100, LinearLayout.LayoutParams.MATCH_PARENT));
                    try{
                        //location_icon.setForegroundGravity(Gravity.CENTER);
                    }catch(Exception e){
                        e.printStackTrace();
                    }
                    location_icon.setPadding(10, 5, 0, 2);
                    location_icon.setImageResource(R.drawable.current_location);
                    location_layout.addView(location_icon);

                    TextView location = new TextView(getContext());
                    location.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT));
                    location.setText(row_dvir.get("location").toString());
                    location.setTypeface(Typeface.SERIF);
                    location.setTextColor(Color.DKGRAY);
                    location.setPadding(10, 5, 0, 2);
                    location.setGravity(Gravity.CENTER_VERTICAL);
                    location.setTextAppearance(getContext(), android.R.style.TextAppearance_DeviceDefault_Medium);
                    location_layout.addView(location);
                    layout.addView(location_layout);

                    LinearLayout vehicle_layout = new LinearLayout(getContext());
                    vehicle_layout.setLayoutParams(params);
                    vehicle_layout.setOrientation(LinearLayout.HORIZONTAL);
                    vehicle_layout.setWeightSum(1);

                    ImageView vehicle_img = new ImageView(getContext());
                    vehicle_img.setLayoutParams(new LinearLayout.LayoutParams(100, LinearLayout.LayoutParams.MATCH_PARENT, 0.2f));
                    vehicle_img.setPadding(8, 10, 8, 10);
                    try {
                        if(android.os.Build.VERSION.SDK_INT >= 21) {
                            vehicle_img.setImageDrawable(getResources().getDrawable(R.drawable.settings_carrier, getContext().getTheme()));
                        } else {
                            vehicle_img.setImageDrawable(getResources().getDrawable(R.drawable.settings_carrier));
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    vehicle_layout.addView(vehicle_img);

                    TextView vehicle_name = new TextView(getContext());
                    vehicle_name.setLayoutParams(params);
                    vehicle_name.setText("Vehicle "+row_dvir.get("vehiclenumber").toString());
                    vehicle_name.setTextAppearance(getContext(), android.R.style.TextAppearance_DeviceDefault_Medium);
                    vehicle_name.setTypeface(Typeface.SERIF);
                    vehicle_name.setTextColor(Color.DKGRAY);
                    vehicle_name.setPadding(10, 5, 0, 2);
                    vehicle_name.setGravity(Gravity.CENTER_VERTICAL);
                    vehicle_layout.addView(vehicle_name);

                    layout.addView(vehicle_layout);

                    JSONArray vehicle_defects_json = new JSONArray(row_dvir.getString("vehicledefects"));
                    if( vehicle_defects_json.length() > 0){
                        for (int it = 0; it < vehicle_defects_json.length(); it++) {
                            LinearLayout defect_layout = new LinearLayout(getContext());
                            defect_layout.setLayoutParams(params);
                            defect_layout.setOrientation(LinearLayout.HORIZONTAL);
                            defect_layout.setWeightSum(1);

                            ImageView defect_img = new ImageView(getContext());
                            defect_img.setLayoutParams(new LinearLayout.LayoutParams(100, LinearLayout.LayoutParams.MATCH_PARENT, 0.2f));
                            defect_img.setPadding(8, 10, 8, 10);
                            try {
                                if(android.os.Build.VERSION.SDK_INT >= 21) {
                                    defect_img.setImageDrawable(getResources().getDrawable(R.drawable.defects, getContext().getTheme()));
                                } else {
                                    defect_img.setImageDrawable(getResources().getDrawable(R.drawable.defects));
                                }
                            }catch(Exception e){
                                e.printStackTrace();
                            }
                            defect_layout.addView(defect_img);

                            TextView defect_text = new TextView(getContext());
                            defect_text.setTextAppearance(getContext(), android.R.style.TextAppearance_DeviceDefault_Small);
                            defect_text.setLayoutParams(params);
                            defect_text.setTextColor(0xFF000000);
                            defect_text.setTypeface(Typeface.SERIF);
                            defect_text.setTextColor(Color.DKGRAY);
                            defect_text.setPadding(10, 5, 0, 2);
                            defect_text.setGravity(Gravity.CENTER_VERTICAL);
                            //table_duration_text.setTextSize(11);
                            defect_text.setText(vehicle_defects_json.getJSONObject(it).get("defects").toString());
                            defect_layout.addView(defect_text);

                            layout.addView(defect_layout);
                        }
                    }else{
                        TextView defect_text = new TextView(getContext());
                        defect_text.setTextAppearance(getContext(), android.R.style.TextAppearance_DeviceDefault_Small);
                        defect_text.setLayoutParams(params);
                        defect_text.setTextColor(0xFF000000);
                        defect_text.setTypeface(Typeface.SERIF);
                        defect_text.setTextColor(Color.DKGRAY);
                        defect_text.setPadding(10, 5, 0, 2);
                        defect_text.setGravity(Gravity.CENTER_VERTICAL);
                        //table_duration_text.setTextSize(11);
                        defect_text.setText("No Vehicle Defects Found");
                        layout.addView(defect_text);
                    }

                    LinearLayout trailer_layout = new LinearLayout(getContext());
                    trailer_layout.setLayoutParams(params);
                    trailer_layout.setOrientation(LinearLayout.HORIZONTAL);
                    trailer_layout.setWeightSum(1);

                    ImageView trailer_img = new ImageView(getContext());
                    trailer_img.setLayoutParams(new LinearLayout.LayoutParams(100, LinearLayout.LayoutParams.MATCH_PARENT, 0.2f));
                    trailer_img.setPadding(8, 10, 8, 10);
                    try {
                        if(android.os.Build.VERSION.SDK_INT >= 21) {
                            trailer_img.setImageDrawable(getResources().getDrawable(R.drawable.trailer, getContext().getTheme()));
                        } else {
                            trailer_img.setImageDrawable(getResources().getDrawable(R.drawable.trailer));
                        }
                    } catch (Exception e){
                        e.printStackTrace();
                    }
                    trailer_layout.addView(trailer_img);

                    TextView trailer_number = new TextView(getContext());
                    trailer_number.setLayoutParams(params);
                    trailer_number.setText(" Trailer "+row_dvir.get("trailernumber").toString());
                    trailer_number.setTextAppearance(getContext(), android.R.style.TextAppearance_DeviceDefault_Medium);
                    trailer_number.setTypeface(Typeface.SERIF);
                    trailer_number.setTextColor(Color.DKGRAY);
                    trailer_number.setPadding(10, 5, 0, 2);
                    trailer_number.setGravity(Gravity.CENTER_VERTICAL);
                    trailer_layout.addView(trailer_number);

                    layout.addView(trailer_layout);

                    JSONArray trailer_defects_json = new JSONArray(row_dvir.getString("trailerdefects"));
                    if( trailer_defects_json.length() > 0 ){
                        for(int it = 0; it < trailer_defects_json.length(); it++) {
                            LinearLayout defect_layout = new LinearLayout(getContext());
                            defect_layout.setLayoutParams(params);
                            defect_layout.setOrientation(LinearLayout.HORIZONTAL);
                            defect_layout.setWeightSum(1);

                            ImageView defect_img = new ImageView(getContext());
                            defect_img.setLayoutParams(new LinearLayout.LayoutParams(100, LinearLayout.LayoutParams.MATCH_PARENT, 0.2f));
                            defect_img.setPadding(8, 10, 8, 10);
                            try {
                                if(android.os.Build.VERSION.SDK_INT >= 21) {
                                    defect_img.setImageDrawable(getResources().getDrawable(R.drawable.defects, getContext().getTheme()));
                                } else {
                                    defect_img.setImageDrawable(getResources().getDrawable(R.drawable.defects));
                                }
                            } catch (Exception e){
                                e.printStackTrace();
                            }
                            defect_layout.addView(defect_img);

                            TextView defect_text = new TextView(getContext());
                            defect_text.setTextAppearance(getContext(), android.R.style.TextAppearance_DeviceDefault_Small);
                            defect_text.setLayoutParams(params);
                            defect_text.setTextColor(0xFF000000);
                            defect_text.setTypeface(Typeface.SERIF);
                            defect_text.setTextColor(Color.DKGRAY);
                            defect_text.setPadding(10, 5, 0, 2);
                            defect_text.setGravity(Gravity.CENTER_VERTICAL);
                            defect_text.setText(trailer_defects_json.getJSONObject(it).get("defects").toString());
                            defect_layout.addView(defect_text);

                            layout.addView(defect_layout);
                        }
                    }else{
                        TextView defect_text = new TextView(getContext());
                        defect_text.setTextAppearance(getContext(), android.R.style.TextAppearance_DeviceDefault_Small);
                        defect_text.setLayoutParams(params);
                        defect_text.setTextColor(0xFF000000);
                        defect_text.setTypeface(Typeface.SERIF);
                        defect_text.setTextColor(Color.DKGRAY);
                        defect_text.setPadding(10, 5, 0, 2);
                        defect_text.setGravity(Gravity.CENTER_VERTICAL);
                        //table_duration_text.setTextSize(11);
                        defect_text.setText("No Trailer Defects Found");
                        layout.addView(defect_text);
                    }

                    LinearLayout def_layout = new LinearLayout(getContext());
                    def_layout.setLayoutParams(params);
                    def_layout.setOrientation(LinearLayout.HORIZONTAL);
                    def_layout.setWeightSum(1);

                    String text = "Defects Need Not Be Corrected";
                    if(row_dvir.getString("defectsCorrected").equals("1")){
                        text = "Defects Corrected";
                    }else if(row_dvir.getString("defectsNotCorrected").equals("1")){
                        text = "Defects Need Not Be Corrected";
                    }

                    TextView def_text = new TextView(getContext());
                    def_text.setTextAppearance(getContext(), android.R.style.TextAppearance_DeviceDefault_Small);
                    def_text.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
                    def_text.setTextColor(0xFF000000);
                    def_text.setTypeface(Typeface.SERIF, Typeface.BOLD_ITALIC);
                    def_text.setTextColor(Color.parseColor("#228B22"));
                    def_text.setPadding(10, 5, 0, 2);
                    def_text.setGravity(Gravity.CENTER);
                    def_text.setText(text);
                    def_layout.addView(def_text);

                    layout.addView(def_layout);

                    LinearLayout sign_layout = new LinearLayout(getContext());
                    sign_layout.setLayoutParams(params);
                    sign_layout.setOrientation(LinearLayout.HORIZONTAL);
                    sign_layout.setWeightSum(1);

                    float weight = 1f;
                    if(row_dvir.getString("driverSigned").equals("1") && row_dvir.getString("mechanicSigned").equals("1")) {
                        weight = 0.5f;

                        ImageView driver_sign = new ImageView(getContext());
                        driver_sign.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT, weight));
                        try {
                            if(android.os.Build.VERSION.SDK_INT >= 21) {
                                driver_sign.setImageDrawable(getResources().getDrawable(R.drawable.tick_driver, null));
                            } else {
                                driver_sign.setImageDrawable(getResources().getDrawable(R.drawable.tick_driver));
                            }
                        } catch(Exception e){
                            e.printStackTrace();
                        }
                        sign_layout.addView(driver_sign);

                        ImageView mechanic_sign = new ImageView(getContext());
                        mechanic_sign.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT, weight));
                        try {
                            if(android.os.Build.VERSION.SDK_INT >= 21) {
                                mechanic_sign.setImageDrawable(getResources().getDrawable(R.drawable.tick_mechanic, null));
                            } else {
                                mechanic_sign.setImageDrawable(getResources().getDrawable(R.drawable.tick_mechanic));
                            }
                        } catch (Exception e){
                            e.printStackTrace();
                        }
                        sign_layout.addView(mechanic_sign);
                    } else {
                        ImageView driver_sign = new ImageView(getContext());
                        driver_sign.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT, weight));
                        try {
                            if(android.os.Build.VERSION.SDK_INT >= 21) {
                                driver_sign.setImageDrawable(getResources().getDrawable(R.drawable.tick_driver, null));
                            } else {
                                driver_sign.setImageDrawable(getResources().getDrawable(R.drawable.tick_driver));
                            }
                        } catch (Exception e){
                            e.printStackTrace();
                        }
                        sign_layout.addView(driver_sign);
                    }

                    layout.addView(sign_layout);

                    // Edit DVIR
                    edit_dvir.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            int cur_dvir_no = Integer.valueOf(dvir_text.getText().toString());
                            HomeActivity.isEdit = true;
                            JSONObject cur_dvir_obj = DBHelper.getDvirData(cur_dvir_no);

                            try {
                                // Time
                                temp_time = cur_dvir_obj.getString("time");

                                // Location
                                temp_location = cur_dvir_obj.getString("location");

                                // Odometer
                                temp_odometer = cur_dvir_obj.getString("odometer");

                                // Carrier
                                temp_carrier = cur_dvir_obj.getString("carrier");

                                // Vehicle number
                                temp_vehicle_no = cur_dvir_obj.getString("vehiclenumber");

                                // Trailer number
                                temp_trailer_no = cur_dvir_obj.getString("trailernumber");

                                // Hours
                                temp_hours = cur_dvir_obj.getString("vehiclehours");

                                // Miles
                                temp_miles = cur_dvir_obj.getString("vehiclemiles");

                                // preInspection
                                temp_inspection_type = Util.getData(cur_dvir_obj.getString("vehicleinspectionType"), 0);

                                // Defects resolved
                                temp_defects_resolved = Util.getData(cur_dvir_obj.getString("defectsCorrected"), 0);

                                // Defects unresolved
                                temp_defects_unresolved = Util.getData(cur_dvir_obj.getString("defectsNotCorrected"), 0);

                                // Mechanic notes
                                temp_mechanic_notes = cur_dvir_obj.getString("mechNotes");

                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            try {
                                // Vehicle defects json
                                temp_vehicle_defects = new JSONArray(cur_dvir_obj.getString("vehicledefects"));

                                // Trailer defects json
                                temp_trailer_defects = new JSONArray(cur_dvir_obj.getString("trailerdefects"));
                                 Log.d("Edit DVIR", "vechile defects: "+temp_vehicle_defects+" trailer defects: "+temp_trailer_defects);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            try{
                                HomeActivity.driverSign = DBHelper.getSignatureBitmap(DBHelper.getLoggedInUserId(), Login.cur_date, temp_time, true);

                                HomeActivity.mechanicSign = DBHelper.getSignatureBitmap(DBHelper.getLoggedInUserId(), Login.cur_date, temp_time, false);
                            } catch (Exception e){
                                e.printStackTrace();
                            }

                            try {
                                // Driver Signed
                                temp_driver_signed = Util.getData(cur_dvir_obj.getString("driverSigned"), 0);

                                // Mechanic Signed
                                temp_mechanic_signed = Util.getData(cur_dvir_obj.getString("mechanicSigned"), 0);
                            } catch (Exception e){
                                e.printStackTrace();
                            }

                            FragmentManager fragmentManager = getFragmentManager();
                            FragmentTransaction transaction = fragmentManager.beginTransaction();
                            transaction.replace(R.id.main_content, new dvir_general(), "dvir_general");
                            transaction.addToBackStack(null);
                            transaction.commitAllowingStateLoss();
                        }
                    });

                    // Delete DVIR
                    delete_dvir.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            final int cur_dvir_no = Integer.valueOf(dvir_text.getText().toString());
                            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            builder.setMessage("Are your sure you want to delete report #"+ cur_dvir_no +" for " + Login.cur_date)
                                    .setCancelable(false)
                                    .setPositiveButton("Delete",new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            DBHelper.deleteDvirData(cur_dvir_no);
                                            Fragment frg = getFragmentManager().findFragmentByTag("DVIR");
                                            final FragmentTransaction ft = getFragmentManager().beginTransaction();
                                            ft.detach(frg);
                                            ft.attach(frg);
                                            ft.commit();
                                            dialog.cancel();
                                        }
                                    })
                                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.cancel();
                                        }
                                    });
                            AlertDialog alert = builder.create();
                            alert.show();
                        }
                    });

                    dvir_layout.addView(layout);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
