package hos.androidhosdvir.settings;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import com.bugfender.sdk.Bugfender;

import java.lang.reflect.Field;

import hos.androidhosdvir.DVIR;
import hos.androidhosdvir.HomeActivity;
import hos.androidhosdvir.R;
import hos.androidhosdvir.db.DBHelper;
import hos.androidhosdvir.util.Util;

public class Settings extends Fragment {

    public static View rootView;
    public static Context context;
    public static String timezone_str;
    public static Spinner timezone;
    public static Button save;
    public static Switch loggingSwitch;

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.settings_logs, container, false);
        HomeActivity.cur_fragment = "settings";
        context = getContext();
        timezone = rootView.findViewById(R.id.timezone);
        save = rootView.findViewById(R.id.logs_save);

        // timezone adapter and selected item
        ArrayAdapter<CharSequence> timezone_adapter = ArrayAdapter.createFromResource(context, R.array.timezones, android.R.layout.simple_spinner_item);
        timezone_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        timezone.setAdapter(timezone_adapter);
        timezone.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                timezone_str = adapterView.getItemAtPosition(i).toString();
                timezone.setSelection(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        // update selected values
        timezone.setSelection(Util.getData(DBHelper.getDefaultHomeTimezone(DBHelper.getLoggedInUserId()) - 1, 0));

        loggingSwitch = (Switch) rootView.findViewById(R.id.loggingSwitch);
        try {
            if(DBHelper.getLoggingValue(DBHelper.getLoggedInUserId())){
                loggingSwitch.setChecked(true);
            } else {
                loggingSwitch.setChecked(false);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
        }

        loggingSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    Toast.makeText(getContext(), "Logging Enabled", Toast.LENGTH_SHORT).show();
                    DBHelper.setLoggingValue(DBHelper.getLoggedInUserId(), true);
                    Bugfender.setForceEnabled(true);
                } else {
                    Toast.makeText(getContext(), "Logging Disabled", Toast.LENGTH_SHORT).show();
                    DBHelper.setLoggingValue(DBHelper.getLoggedInUserId(), false);
                    Bugfender.setForceEnabled(false);
                }
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int timezoneid = (int) timezone.getSelectedItemId() + 1;
                DBHelper.updateTimezone(timezoneid, DBHelper.getLoggedInUserId());
                Toast.makeText(context, "Saved", Toast.LENGTH_SHORT).show();
            }
        });

        ImageView back = (ImageView) rootView.findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!getActivity().isFinishing()) {
                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction transaction = fragmentManager.beginTransaction();
                    transaction.replace(R.id.main_content, new DVIR(), "DVIR");
                    transaction.addToBackStack(null);
                    transaction.commitAllowingStateLoss();
                }
            }
        });

        return rootView;
    }

    @Override
    public void onDetach() {
        super.onDetach();

        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);

        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }
}