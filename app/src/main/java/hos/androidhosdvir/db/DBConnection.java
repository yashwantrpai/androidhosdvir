package hos.androidhosdvir.db;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DBConnection extends SQLiteOpenHelper {

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 5;

    //The Android's default system path of your application database.
    private static String DB_PATH = "/data/data/hos.hosgeneric/databases/";

    public static String DBNAME = "dvir.db";

    private SQLiteDatabase myDataBase;
    public static Context context;
    public DBConnection(Context context) {
        super(context, DBNAME, null, DATABASE_VERSION);
        this.context = context;
    }

    // closing database
    public void closeDB() {
        SQLiteDatabase db = this.getReadableDatabase();
        if (db != null && db.isOpen())
            db.close();
    }

    @Override
    public synchronized void close() {

        if(myDataBase != null)
            myDataBase.close();

        super.close();
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        DBHelper.create(sqLiteDatabase);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    public SQLiteDatabase getWritableDatabaseInstance() {
        return this.getWritableDatabase();
    }

    public SQLiteDatabase getReadableDatabaseInstance() {
        return this.getReadableDatabase();
    }
}

