package hos.androidhosdvir.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.util.Pair;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import hos.androidhosdvir.App;
import hos.androidhosdvir.DVIR;
import hos.androidhosdvir.HomeActivity;
import hos.androidhosdvir.util.Util;

public class DBHelper {

    public static final String dvir_login = "dvir_login";
    public static final String dvir_data = "dvir_data";
    public static final String dvir_sign = "dvir_sign";

    public static void create(SQLiteDatabase db) {
        Cursor res = db.rawQuery("SELECT name FROM sqlite_master WHERE type='table' AND name='"+dvir_login+"'", null);
        if(res.getCount() == 0){
            db.execSQL("CREATE TABLE " + dvir_login + "(id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    "flag INTEGER, " +
                    "username VARCHAR, " +
                    "password VARCHAR, " +
                    "timezone INTEGER DEFAULT 1," +
                    "loggingSwitch INTEGER)");
        }

        res = db.rawQuery("SELECT name FROM sqlite_master WHERE type='table' AND name='"+dvir_data+"'", null);
        if(res.getCount() == 0){
            db.execSQL("CREATE TABLE " + dvir_data + "(id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    "userid INTEGER, " +
                    "date VARCHAR, " +
                    "time VARCHAR, " +
                    "location VARCHAR, " +
                    "defectsCorrected VARCHAR, " +
                    "defectsNotCorrected VARCHAR, " +
                    "carrier VARCHAR, " +
                    "odometer VARCHAR, " +
                    "driverSigned VARCHAR, " +
                    "mechanicSigned VARCHAR, " +
                    "mechNotes VARCHAR, " +
                    "vehiclenumber VARCHAR, " +
                    "vehicledefects VARCHAR, " +
                    "vehiclehours VARCHAR, " +
                    "vehiclemiles VARCHAR, " +
                    "vehicleinspectionType VARCHAR, " +
                    "trailernumber VARCHAR, " +
                    "trailerdefects VARCHAR)");
        }

        res = db.rawQuery("SELECT name FROM sqlite_master WHERE type='table' AND name='"+dvir_sign+"'", null);
        if(res.getCount() == 0){
            db.execSQL("CREATE TABLE " + dvir_sign + "(id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    "userid INTEGER, " +
                    "date VARCHAR, " +
                    "time VARCHAR, " +
                    "driverSign VARCHAR, " +
                    "mechSign VARCHAR);");
        }
    }

    public static boolean login(String username, String password){
        long res = -1;
        SQLiteDatabase db = App.db.getWritableDatabaseInstance();
        String args[] = {username};
        try{
            ContentValues contentValues = new ContentValues();
            contentValues.put("username", username);
            contentValues.put("password", password);
            contentValues.put("flag", 1);
            res = db.update(dvir_login, contentValues, "username = ?", args);
            if(res == 0 || res == -1){
                res = db.insert(dvir_login, "", contentValues);
            }

        } catch(Exception e){
            e.printStackTrace();
        }

        return (res != -1 && res != 0)?true:false;
    }

    public static boolean logout(int userid){
        long res = -1;
        SQLiteDatabase db = App.db.getWritableDatabaseInstance();
        String args[] = {String.valueOf(userid)};
        try{
            ContentValues contentValues = new ContentValues();
            contentValues.put("flag", 0);
            res = db.update(dvir_login, contentValues, "id = ?", args);

        } catch(Exception e){
            e.printStackTrace();
        }

        return (res != -1 && res != 0)?true:false;
    }

    public static JSONArray getDvirData(int userid, String date){
        SQLiteDatabase db = App.db.getReadableDatabaseInstance();
        JSONArray dvirData = new JSONArray();

        Cursor cursor = null;
        try{
            cursor = db.rawQuery("select * from "+ dvir_data +" where userid=" + userid + " AND date = '"+ date +"';", null);
            if (cursor != null && cursor.moveToFirst()) {
                int i = 0;
                while ( i < cursor.getCount()) {
                    JSONObject obj = new JSONObject();
                    obj = new JSONObject();
                    obj.put("userid", cursor.getString(cursor.getColumnIndex("userid")));
                    obj.put("dvirid", cursor.getString(cursor.getColumnIndex("id")));
                    obj.put("date", cursor.getString(cursor.getColumnIndex("date")));
                    obj.put("time", cursor.getString(cursor.getColumnIndex("time")));
                    obj.put("location", cursor.getString(cursor.getColumnIndex("location")));
                    obj.put("defectsCorrected", cursor.getString(cursor.getColumnIndex("defectsCorrected")));
                    obj.put("defectsNotCorrected", cursor.getString(cursor.getColumnIndex("defectsNotCorrected")));
                    obj.put("carrier", cursor.getString(cursor.getColumnIndex("carrier")));
                    obj.put("odometer", cursor.getString(cursor.getColumnIndex("odometer")));
                    obj.put("driverSigned", cursor.getString(cursor.getColumnIndex("driverSigned")));
                    obj.put("mechanicSigned", cursor.getString(cursor.getColumnIndex("mechanicSigned")));
                    obj.put("mechNotes", cursor.getString(cursor.getColumnIndex("mechNotes")));
                    obj.put("vehiclenumber", cursor.getString(cursor.getColumnIndex("vehiclenumber")));
                    obj.put("vehicledefects", cursor.getString(cursor.getColumnIndex("vehicledefects")));
                    obj.put("vehiclehours", cursor.getString(cursor.getColumnIndex("vehiclehours")));
                    obj.put("vehiclemiles", cursor.getString(cursor.getColumnIndex("vehiclemiles")));
                    obj.put("vehicleinspectionType", cursor.getString(cursor.getColumnIndex("vehicleinspectionType")));
                    obj.put("trailernumber", cursor.getString(cursor.getColumnIndex("trailernumber")));
                    obj.put("trailerdefects", cursor.getString(cursor.getColumnIndex("trailerdefects")));
                    dvirData.put(obj);
                    i++;
                    cursor.moveToNext();
                }
            }
        } catch (Exception e){
            e.printStackTrace();
        }

        return dvirData;
    }

    public static JSONObject getDvirData(int dvirid){
        SQLiteDatabase db = App.db.getReadableDatabaseInstance();
        JSONObject obj = new JSONObject();

        Cursor cursor = null;
        try{
            cursor = db.rawQuery("select * from "+ dvir_data +" where id=" + dvirid +";", null);
            if (cursor != null && cursor.moveToFirst()) {
                int i = 0;
                while ( i < cursor.getCount()) {
                    obj = new JSONObject();
                    obj.put("userid", cursor.getString(cursor.getColumnIndex("userid")));
                    obj.put("dvirid", cursor.getString(cursor.getColumnIndex("id")));
                    obj.put("date", cursor.getString(cursor.getColumnIndex("date")));
                    obj.put("time", cursor.getString(cursor.getColumnIndex("time")));
                    obj.put("location", cursor.getString(cursor.getColumnIndex("location")));
                    obj.put("defectsCorrected", cursor.getString(cursor.getColumnIndex("defectsCorrected")));
                    obj.put("defectsNotCorrected", cursor.getString(cursor.getColumnIndex("defectsNotCorrected")));
                    obj.put("carrier", cursor.getString(cursor.getColumnIndex("carrier")));
                    obj.put("odometer", cursor.getString(cursor.getColumnIndex("odometer")));
                    obj.put("driverSigned", cursor.getString(cursor.getColumnIndex("driverSigned")));
                    obj.put("mechanicSigned", cursor.getString(cursor.getColumnIndex("mechanicSigned")));
                    obj.put("mechNotes", cursor.getString(cursor.getColumnIndex("mechNotes")));
                    obj.put("vehiclenumber", cursor.getString(cursor.getColumnIndex("vehiclenumber")));
                    obj.put("vehicledefects", cursor.getString(cursor.getColumnIndex("vehicledefects")));
                    obj.put("vehiclehours", cursor.getString(cursor.getColumnIndex("vehiclehours")));
                    obj.put("vehiclemiles", cursor.getString(cursor.getColumnIndex("vehiclemiles")));
                    obj.put("vehicleinspectionType", cursor.getString(cursor.getColumnIndex("vehicleinspectionType")));
                    obj.put("trailernumber", cursor.getString(cursor.getColumnIndex("trailernumber")));
                    obj.put("trailerdefects", cursor.getString(cursor.getColumnIndex("trailerdefects")));
                    i++;
                    cursor.moveToNext();
                }
            }
        } catch (Exception e){
            e.printStackTrace();
        }

        return obj;
    }

    public static boolean updateDvirData(JSONObject dvirObject){
        SQLiteDatabase db = App.db.getWritableDatabaseInstance();
        long res = 0;
        String args[] = new String[0];
        try {
            args = new String[]{dvirObject.getString("userid"), dvirObject.getString("date"), dvirObject.getString("time")};
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try{
            ContentValues contentValues = new ContentValues();
            contentValues.put("userid", dvirObject.getInt("userid"));
            contentValues.put("date", dvirObject.getString("date"));
            contentValues.put("time", dvirObject.getString("time"));
            contentValues.put("carrier", dvirObject.getString("carrier"));
            contentValues.put("location", dvirObject.getString("location"));
            contentValues.put("odometer", dvirObject.getString("odometer"));
            contentValues.put("vehiclenumber", dvirObject.getString("vehiclenumber"));
            contentValues.put("vehiclehours", dvirObject.getString("vehiclehours"));
            contentValues.put("vehiclemiles", dvirObject.getString("vehiclemiles"));
            contentValues.put("vehicleinspectionType", dvirObject.getString("vehicleinspectionType"));
            contentValues.put("vehicledefects", dvirObject.getString("vehicledefects"));
            contentValues.put("trailernumber", dvirObject.getString("trailernumber"));
            contentValues.put("trailerdefects", dvirObject.getString("trailerdefects"));
            contentValues.put("defectsCorrected", dvirObject.getString("defectsCorrected"));
            contentValues.put("defectsNotCorrected", dvirObject.getString("defectsNotCorrected"));
            contentValues.put("driverSigned", dvirObject.getString("driverSigned"));
            contentValues.put("mechanicSigned", dvirObject.getString("mechanicSigned"));
            contentValues.put("mechNotes", dvirObject.getString("mechNotes"));
            res = db.update(dvir_data, contentValues, "userid = ? AND date = ? AND time = ?", args);
            if(res == 0 || res == -1){
                res = db.insert(dvir_data, "", contentValues);
            }
        } catch(Exception e){
            e.printStackTrace();
        }

        return (res != -1 && res != 0)?true:false;
    }

    public static boolean updateTimezone(int timezone, int userid){
        SQLiteDatabase db = App.db.getWritableDatabaseInstance();
        long res = 0;
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put("timezone", timezone);
            String args[] = {String.valueOf(userid)};
            res = db.update(dvir_login, contentValues, "id = ?", args);
        } catch (Exception e){
            e.printStackTrace();
        }

        return (res == 0 || res == -1)?false:true;
    }

    public static boolean deleteDvirData(int dvirid){
        SQLiteDatabase db = App.db.getWritableDatabaseInstance();
        long res = 0;
        String args[] = {String.valueOf(dvirid)};
        try {
            res = db.delete(dvir_data, "id = ?", args);
        } catch (Exception e){
            e.printStackTrace();
        }

        return (res == 0 || res == -1)?false:true;
    }

    public static boolean saveSignatureBitmap(JSONObject signatureObject){
        long res = -1;

        SQLiteDatabase db = App.db.getWritableDatabaseInstance();
        try{
            ContentValues contentValues = new ContentValues();
            contentValues.put("userid", signatureObject.getString("userid"));
            contentValues.put("date", signatureObject.getString("date"));
            contentValues.put("time", signatureObject.getString("time"));
            contentValues.put("driverSign", signatureObject.getString("driverSign"));
            contentValues.put("mechSign", signatureObject.getString("mechSign"));

            String[] whereArgs = {signatureObject.getString("userid"), signatureObject.getString("date"), signatureObject.getString("time")};
            res = db.update(dvir_sign, contentValues, "userid = ? AND date = ? AND time = ?", whereArgs);
            if(res == 0){
                res = db.insert(dvir_sign, "", contentValues);
            }
        } catch (Exception e){
            e.printStackTrace();
            res = 0;
        }

        return (res > 0)?true:false;
    }

    public static String getSignatureBitmap(int userid, String date, String time, boolean driverSign){
        String bitmap = "";
        long result = 0;
        String sign = (driverSign)?"driverSign":"mechSign";
        SQLiteDatabase db = App.db.getWritableDatabaseInstance();
        try{
            Cursor cursor = db.rawQuery("SELECT "+ sign +" FROM " + dvir_sign + " WHERE userid = " + userid + " AND date = '"+ date + "' AND time = '"+ time +"';", null);
            if((cursor != null) && cursor.moveToFirst()) {
                bitmap = cursor.getString(cursor.getColumnIndex(sign));
                result = 1;
            }
            cursor.close();
        } catch (Exception e){
            e.printStackTrace();
            result = 0;
        }
        //db.close();
        return bitmap;
    }

    public static boolean getLoggingValue(int userid) throws Exception {
        boolean ret = false;
        Cursor cursor = null;
        SQLiteDatabase db = App.getDbConnection().getReadableDatabaseInstance();
        try{
            cursor = db.rawQuery("SELECT loggingSwitch FROM dvir_login WHERE id = "+ userid, null);
            if ((cursor != null) && cursor.moveToFirst()) {
                if (cursor.getString(cursor.getColumnIndex("loggingSwitch")).equals("1")) {
                    ret = true;
                } else {
                    ret = false;
                }
            }
        } catch (Exception ex){
            Log.e("DatabaseHelper", "Debug switch issue", ex);
            ex.printStackTrace();
        }
        if (cursor != null) {
            cursor.close();
        }
        //db.close();
        return ret;
    }

    public static boolean setLoggingValue(int userid, boolean val){
        boolean ret = false;
        SQLiteDatabase db = App.getDbConnection().getWritableDatabaseInstance();
        try{
            Cursor cursor = db.rawQuery("UPDATE dvir_login SET loggingSwitch = "+ (val?1:0) +" WHERE id = "+ userid, null);
            if ((cursor != null) && cursor.moveToFirst()) {
                if (cursor.getString(cursor.getColumnIndex("loggingSwitch")).equals("1")) {
                    ret = true;
                } else {
                    ret = false;
                }
            }
            cursor.close();
        } catch (Exception e){
            e.printStackTrace();
        }
        //db.close();
        return ret;
    }

    public static String getLoggedInUser(){              // Returns username of user currently logged in
        String user = "";
        SQLiteDatabase db = App.db.getWritableDatabaseInstance();
        try{
            Cursor cursor = db.rawQuery("SELECT username FROM dvir_login WHERE flag = 1;", null);
            if(cursor != null && cursor.moveToFirst()){
                user = cursor.getString(cursor.getColumnIndex("username"));
            }
            cursor.close();
        } catch(Exception e){
            e.printStackTrace();
        }

        //db.close();
        return user;
    }

    public static int getLoggedInUserId(){              // Returns id of user currently logged in
       int userid = 0;
        SQLiteDatabase db = App.db.getReadableDatabaseInstance();
        try{
            Cursor cursor = db.rawQuery("SELECT id FROM dvir_login WHERE flag = 1;", null);
            if(cursor != null && cursor.moveToFirst()){
                userid = cursor.getInt(cursor.getColumnIndex("id"));
            }
            cursor.close();
        } catch(Exception e){
            e.printStackTrace();
        }

        //db.close();
        return userid;
    }

    public static String getLoggedInUserDefaultTimezoneInGTM() {
        String str = Util.getTimezoneGTMStr(getDefaultHomeTimezone(getLoggedInUserId()));
        if (str != null && !str.isEmpty()){
            if (str.equalsIgnoreCase("NA")) {
                str = "GMT-8:00";
            }
        }
        return str;
    }

    public static int getDefaultHomeTimezone(int userid){
        int timezone = 1;
        SQLiteDatabase db = App.getDbConnection().getReadableDatabaseInstance();
        try{
            Cursor cursor = db.rawQuery("SELECT timezone FROM " + dvir_login + " WHERE id = " + userid + ";", null);
            if(cursor != null && cursor.moveToFirst()){
                timezone = cursor.getInt(cursor.getColumnIndex("timezone"));
            }
            cursor.close();
        } catch(Exception e){
            e.printStackTrace();
        }
        //db.close(); */
        return timezone;
    }

    public static String getDefaultHomeTimezoneinGTM(int userid) {
        return Util.getTimezoneGTMStr(getDefaultHomeTimezone(userid));
    }
}
