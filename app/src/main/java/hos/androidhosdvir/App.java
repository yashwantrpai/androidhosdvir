package hos.androidhosdvir;

import android.app.Application;
import android.content.Context;

import com.bugfender.sdk.Bugfender;
import com.crashlytics.android.Crashlytics;

import hos.androidhosdvir.db.DBConnection;
import io.fabric.sdk.android.*;

public class App extends Application {

    public static DBConnection db;
    public static Context appContext;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());

        Bugfender.init(this, "bG4zrnHRAYHWhryQxfnvFb00gKp2eYpR", BuildConfig.DEBUG);
        Bugfender.enableLogcatLogging();
        Bugfender.enableUIEventLogging(this);
        Bugfender.enableCrashReporting();
        Crashlytics.setUserIdentifier(Bugfender.getDeviceIdentifier());

        appContext = this;
        db = new DBConnection(appContext);
    }

    public static Context getContext(){
        return appContext;
    }

    public static DBConnection getDbConnection(){
        return db;
    }

    @Override
    public void onTerminate() {
        db.closeDB();
        super.onTerminate();
    }
}