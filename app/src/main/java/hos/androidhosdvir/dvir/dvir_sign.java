package hos.androidhosdvir.dvir;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import hos.androidhosdvir.DVIR;
import hos.androidhosdvir.Defects;
import hos.androidhosdvir.HomeActivity;
import hos.androidhosdvir.Login;
import hos.androidhosdvir.R;
import hos.androidhosdvir.Sign;
import hos.androidhosdvir.db.DBHelper;
import hos.androidhosdvir.util.Util;

/**
 * A simple {@link Fragment} subclass.
 */
public class dvir_sign extends Fragment {

    public dvir_sign() {
        // Required empty public constructor
    }
    // public ArrayList<Defects> defect_resolved_list;
    // public MyCustomAdapter.ViewHolder holder = null;

    ConnectivityManager connectivityManager;
    ArrayList<Pair> dvir_arr;
    Pair<String, String> pair;
    ImageView driver_sign_img, mechanic_sign_img;
    Button driver_sign_log_btn, mechanic_sign_log_btn;
    // public static ArrayList<Defects> defectsList = new ArrayList<Defects>();
    public static JSONObject DVIRJSONObj = new JSONObject();
    public static TextView mechanic_notes;
    public static RadioGroup defectsRadioGroup;
    public static TextView no_defects;
    public static RadioButton defectsCorrected, defectsNotCorrected;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_dvir_sign, container, false);
        connectivityManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        driver_sign_img = (ImageView) rootView.findViewById(R.id.driver_sign_img);
        mechanic_sign_img = (ImageView) rootView.findViewById(R.id.mechanic_sign_img);
        driver_sign_log_btn = (Button) rootView.findViewById(R.id.driver_sign_log_btn);
        mechanic_sign_log_btn = (Button) rootView.findViewById(R.id.mechanic_sign_log_btn);
        mechanic_notes = (TextView) rootView.findViewById(R.id.mechanic_notes);
        defectsRadioGroup = (RadioGroup) rootView.findViewById(R.id.defectsRadioGroup);
        no_defects = (TextView) rootView.findViewById(R.id.no_defects);
        defectsCorrected = (RadioButton) rootView.findViewById(R.id.defectsCorrected);
        defectsNotCorrected = (RadioButton) rootView.findViewById(R.id.defectsNotCorrected);

        defectsCorrected.setChecked(false);
        defectsNotCorrected.setChecked(false);

        // Sign.sign_prev = "";
        HomeActivity.cur_fragment = "dvir_sign";
        // retain this fragment
        setRetainInstance(true);

        Button sign_btn = (Button) rootView.findViewById(R.id.sign_dvir_btn);
        sign_btn.setSelected(true);
        Boolean resolved = false, unresolved = false;

        resolved = DVIR.temp_defects_resolved==1?true:false;
        unresolved = DVIR.temp_defects_unresolved==1?true:false;
        driver_sign_img.setImageBitmap(Util.StringToBitMap(HomeActivity.driverSign));
        mechanic_sign_img.setImageBitmap(Util.StringToBitMap(HomeActivity.mechanicSign));
        mechanic_notes.setText(DVIR.temp_mechanic_notes);

        if(resolved){
            defectsCorrected.setChecked(true);
            defectsNotCorrected.setChecked(false);
        } else if(unresolved){
            defectsNotCorrected.setChecked(true);
            defectsCorrected.setChecked(false);
        }

        ImageView back = (ImageView) rootView.findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DVIR.temp_defects_resolved = 0;
                DVIR.temp_defects_unresolved = 0;
                DVIR.temp_driver_signed = 0;
                DVIR.temp_mechanic_signed = 0;
                DVIR.temp_mechanic_notes = mechanic_notes.getText().toString();

                if(!getActivity().isFinishing()) {
                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction transaction = fragmentManager.beginTransaction();
                    transaction.replace(R.id.main_content, new DVIR(), "DVIR");
                    transaction.addToBackStack(null);
                    transaction.commitAllowingStateLoss();
                }
            }
        });

        final Button vehicle_btn = (Button) rootView.findViewById(R.id.vehicle_dvir_btn);
        vehicle_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DVIR.temp_mechanic_notes = mechanic_notes.getText().toString();
                if(!getActivity().isFinishing()) {
                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction transaction = fragmentManager.beginTransaction();
                    transaction.replace(R.id.main_content, new dvir_vehicle(), "dvir_vehicle");
                    transaction.addToBackStack(null);
                    transaction.commitAllowingStateLoss();
                }
            }
        });

        Button general_btn = (Button) rootView.findViewById(R.id.general_dvir_btn);
        general_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DVIR.temp_mechanic_notes = mechanic_notes.getText().toString();
                if(!getActivity().isFinishing()) {
                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction transaction = fragmentManager.beginTransaction();
                    transaction.replace(R.id.main_content, new dvir_general(), "dvir_general");
                    transaction.addToBackStack(null);
                    transaction.commitAllowingStateLoss();
                }
            }
        });

        final LinearLayout defects_layout = (LinearLayout) rootView.findViewById(R.id.defect_layout);
        if((DVIR.vehicleDefects.size() > 0) || (DVIR.trailerDefects.size() > 0)){
            if(defects_layout.getChildCount() > 0){
                defects_layout.removeAllViews();
            }

            defects_layout.addView(defectsRadioGroup);
        } else if(DVIR.vehicleDefects.size() == 0 && DVIR.trailerDefects.size() == 0){
            if(defects_layout.getChildCount() > 0){
                defects_layout.removeAllViews();
            }
            defects_layout.addView(no_defects);
        }

        defectsCorrected.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    DVIR.temp_defects_resolved = 1;
                    DVIR.temp_defects_unresolved = 0;
                }
            }
        });

        defectsNotCorrected.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    DVIR.temp_defects_unresolved = 1;
                    DVIR.temp_defects_resolved = 0;
                }
            }
        });

        Button new_dvir = (Button) rootView.findViewById(R.id.new_dvir);
        new_dvir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DVIR.temp_mechanic_notes = mechanic_notes.getText().toString();
                    if (dvir_vehicle.vehicle_number == null || dvir_vehicle.vehicle_number.getText().toString() == "") {
                        Toast.makeText(getContext(), "Vehicle Number required for DVIR", Toast.LENGTH_SHORT).show();
                    } else if(DVIR.temp_driver_signed != 1){
                        Toast.makeText(getContext(), "Driver signature required for DVIR", Toast.LENGTH_SHORT).show();
                    } else {
                        JSONArray vehicleArr = new JSONArray();
                        JSONArray trailerArr = new JSONArray();
                        try {
                            String defects_array[] = getResources().getStringArray(R.array.defects_array);
                            JSONObject vehicle_defects_json = new JSONObject(), trailer_defects_json = new JSONObject();

                            if (DVIR.vehicleDefects.size() != 0) {
                                for (int i = 0; i < DVIR.vehicleDefects.size(); i++) {
                                    vehicle_defects_json.put(String.valueOf(i), String.valueOf(DVIR.vehicleDefects.get(i).isSelected()));
                                    if (DVIR.vehicleDefects.get(i).isSelected()) {
                                        JSONObject defectObj = new JSONObject();
                                        defectObj.put("id", DVIR.vehicleDefects.get(i).getCode());
                                        defectObj.put("defects", DVIR.vehicleDefects.get(i).getName());
                                        defectObj.put("info", DVIR.vehicleDefects.get(i).getInfo());
                                        vehicleArr.put(defectObj);
                                    }
                                }
                            } else {
                                for (int i = 0; i < defects_array.length; i++) {
                                    vehicle_defects_json.put(String.valueOf(i), "false");
                                }
                            }

                            if (DVIR.trailerDefects.size() != 0) {
                                for (int i = 0; i < DVIR.trailerDefects.size(); i++) {
                                    trailer_defects_json.put(String.valueOf(i), DVIR.trailerDefects.get(i).isSelected());
                                    if (DVIR.trailerDefects.get(i).isSelected()) {
                                        JSONObject defectObj = new JSONObject();
                                        defectObj.put("id", DVIR.trailerDefects.get(i).getCode());
                                        defectObj.put("defects", DVIR.trailerDefects.get(i).getName());
                                        defectObj.put("info", DVIR.trailerDefects.get(i).getInfo());
                                        trailerArr.put(defectObj);
                                    }
                                }
                            } else {
                                for (int i = 0; i < DVIR.trailer_defect_ind.length; i++) {
                                    trailer_defects_json.put(String.valueOf(i), "false");
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        DVIR.isVehicleEdited = false;
                        DVIR.isTrailerEdited = false;


                        JSONObject object = new JSONObject();
                        try {
                            object.put("userid", DBHelper.getLoggedInUserId());
                            object.put("date", Login.cur_date);
                            object.put("time", DVIR.temp_time);
                            object.put("carrier", DVIR.temp_carrier);
                            object.put("location", DVIR.temp_location);
                            object.put("odometer", DVIR.temp_odometer);
                            object.put("vehiclenumber", DVIR.temp_vehicle_no);
                            object.put("vehiclehours", DVIR.temp_hours);
                            object.put("vehiclemiles", DVIR.temp_miles);
                            object.put("vehicleinspectionType", DVIR.temp_inspection_type);
                            object.put("vehicledefects", vehicleArr);
                            object.put("trailernumber", DVIR.temp_trailer_no);
                            object.put("trailerdefects", trailerArr);
                            object.put("defectsCorrected", Integer.toString(DVIR.temp_defects_resolved));
                            object.put("defectsNotCorrected", Integer.toString(DVIR.temp_defects_unresolved));
                            object.put("driverSigned", Integer.toString(DVIR.temp_driver_signed));
                            object.put("mechanicSigned", Integer.toString(DVIR.temp_mechanic_signed));
                            object.put("mechNotes", mechanic_notes.getText().toString().trim());
                            DBHelper.updateDvirData(object);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        object = new JSONObject();
                        try{
                            object.put("userid", DBHelper.getLoggedInUserId());
                            object.put("date", Login.cur_date);
                            object.put("time", DVIR.temp_time);
                            object.put("driverSign", HomeActivity.driverSign);
                            object.put("mechSign", HomeActivity.mechanicSign);
                            DBHelper.saveSignatureBitmap(object);
                        }catch (Exception e){
                            e.printStackTrace();
                        }

                        Date today = new Date();
                        SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
                        String dt = df.format(today);
                        df = new SimpleDateFormat("HH:mm:ss");
                        Toast.makeText(getContext(), "DVIR saved for " + Login.cur_date + " at " + df.format(today), Toast.LENGTH_SHORT).show();

                        // action: 0 - save, 1 - edit, 2 - delete
                        int action = HomeActivity.isEdit ? 1 : 0;
                    }
            }
        });

        driver_sign_log_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DVIR.temp_mechanic_notes = mechanic_notes.getText().toString();
                HomeActivity.person = 0;
                DVIR.temp_driver_signed = 1;
                Intent i = new Intent(getContext(), Sign.class);
                startActivity(i);
            }
        });

        mechanic_sign_log_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DVIR.temp_mechanic_notes = mechanic_notes.getText().toString();
                HomeActivity.person = 1;
                DVIR.temp_mechanic_signed = 1;
                Intent i = new Intent(getContext(), Sign.class);
                startActivity(i);
            }
        });

        return rootView;
    }

    @Override
    public void onDetach() {
        super.onDetach();

        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);

        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }
}
