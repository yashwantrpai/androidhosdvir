package hos.androidhosdvir.dvir;

/**
 * A simple {@link Fragment} subclass.
 */
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.content.Context;
import android.util.Log;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

import java.lang.reflect.Field;
import java.util.ArrayList;

import hos.androidhosdvir.DVIR;
import hos.androidhosdvir.Defects;
import hos.androidhosdvir.HomeActivity;
import hos.androidhosdvir.R;

public class defects extends Fragment {

    public MyCustomAdapter dataAdapter = null;
    public MyCustomAdapter.ViewHolder holder = null;
    View rootView;
    Context context;
    public static ArrayList<Defects> defectList;

    public defects(){

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        rootView = inflater.inflate(R.layout.defects, container, false);
        context = getContext();
        Button done_btn = (Button) rootView.findViewById(R.id.defects_done_btn);
        HomeActivity.cur_fragment = "defects";
        // retain this fragment
        setRetainInstance(true);
        TextView heading = (TextView) rootView.findViewById(R.id.heading);

        if(dvir_vehicle.isVehicle) {
            heading.setText("Vehicle Defects");
            DVIR.isVehicleEdited = true;
        }
        else {
            heading.setText("Trailer Defects");
            DVIR.isTrailerEdited  = true;
        }

        // Log.d("IsEdit", MainActivity.isEdit.toString());
        //Generate list View from ArrayList
        displayListView();

        done_btn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(dvir_vehicle.isVehicle) {
                    DVIR.vehicleDefects = dataAdapter.defectsList;
                }
                else {
                    DVIR.trailerDefects = dataAdapter.defectsList;
                }

                if(!getActivity().isFinishing()) {
                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction transaction = fragmentManager.beginTransaction();
                    transaction.replace(R.id.main_content, new dvir_vehicle(), "dvir_vehicle");
                    transaction.addToBackStack(null);
                    transaction.commitAllowingStateLoss();
                }
            }
        });

        ImageView back = (ImageView) rootView.findViewById(R.id.back);
        back.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!getActivity().isFinishing()) {
                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction transaction = fragmentManager.beginTransaction();
                    transaction.replace(R.id.main_content, new dvir_vehicle(), "dvir_vehicle");
                    transaction.addToBackStack(null);
                    transaction.commitAllowingStateLoss();
                }
            }
        });

        return rootView;
    }

    @Override
    public void onDetach() {
        super.onDetach();

        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);

        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    private void displayListView()
    {
        // Vehicle defects
        defectList = new ArrayList<Defects>();
        Defects _defects;
        String defects_array[] = getResources().getStringArray(R.array.defects_array);

        if(!HomeActivity.isEdit ) { // Add DVIR case
            if (dvir_vehicle.isVehicle) {
                if(DVIR.vehicleDefects.size() == 0) {
                    for (int i = 0; i < defects_array.length; i++) {
                        _defects = new Defects(String.valueOf(i), defects_array[i], "", false);
                        defectList.add(_defects);
                    }
                } else {
                    defectList = DVIR.vehicleDefects;
                }
            } else {
                if(DVIR.trailerDefects.size() == 0) {
                    for (int i = 0; i < DVIR.trailer_defect_ind.length; i++) {
                        _defects = new Defects(String.valueOf(DVIR.trailer_defect_ind[i]), defects_array[DVIR.trailer_defect_ind[i]], "", false);
                        defectList.add(_defects);
                    }
                } else {
                    defectList = DVIR.trailerDefects;
                }
            }
        }else{  // Edit DVIR case
            if (dvir_vehicle.isVehicle) {
                if(DVIR.vehicleDefects == null || DVIR.vehicleDefects.isEmpty() || DVIR.vehicleDefects.size() == 0) {
                    for (int i = 0; i < defects_array.length; i++) {
                        _defects = new Defects(String.valueOf(i), defects_array[i], "", false);
                        defectList.add(_defects);
                    }
                } else {
                    defectList = DVIR.vehicleDefects;
                }
            }else{
                if(DVIR.trailerDefects == null || DVIR.trailerDefects.isEmpty() || DVIR.trailerDefects.size() == 0) {
                    for (int i = 0; i < DVIR.trailer_defect_ind.length; i++) {
                        _defects = new Defects(String.valueOf(DVIR.trailer_defect_ind[i]), defects_array[DVIR.trailer_defect_ind[i]], "", false);
                        defectList.add(_defects);
                    }
                } else {
                    defectList = DVIR.trailerDefects;
                }
            }
        }

        // Log.d("Defect List", defectList.toString());
        //create an ArrayAdapter from the String Array
        dataAdapter = new MyCustomAdapter(context, R.layout.defect_info, defectList);
        ListView listView = (ListView) rootView.findViewById(R.id.listView1);
        // Assign adapter to ListView
        listView.setAdapter(dataAdapter);

        listView.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // When clicked, show a toast with the TextView text
                Defects defects = (Defects) parent.getItemAtPosition(position);
            }
        });
    }

    public class MyCustomAdapter extends ArrayAdapter<Defects>
    {
        private ArrayList<Defects> defectsList;

        public MyCustomAdapter(Context context, int textViewResourceId, ArrayList<Defects> defectsList)
        {
            super(context, textViewResourceId, defectsList);
            this.defectsList = new ArrayList<Defects>();
            this.defectsList.addAll(defectsList);
        }

        private class ViewHolder
        {
            //TextView code;
            CheckBox name;
            EditText info;
            ImageView asterisk;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            Log.v("ConvertView", String.valueOf(position));

            if (convertView == null)
            {
                LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                convertView = vi.inflate(R.layout.defect_info, null);

                holder = new ViewHolder();
                //holder.code = (TextView) convertView.findViewById(R.id.code);
                holder.name = (CheckBox) convertView.findViewById(R.id.checkBox1);
                holder.info = (EditText) convertView.findViewById(R.id.info1);
                holder.asterisk = (ImageView) convertView.findViewById(R.id.asterisk);

                convertView.setTag(holder);

                holder.name.setOnClickListener( new View.OnClickListener()
                {
                    public void onClick(View v)
                    {
                        CheckBox cb = (CheckBox) v;
                        Defects _defect = (Defects) cb.getTag();
                        // if(cb.isChecked())
                            // Toast.makeText(context, cb.getText() + " checked", Toast.LENGTH_SHORT).show();
                        // else
                            // Toast.makeText(context, cb.getText() + " unchecked", Toast.LENGTH_SHORT).show();
                        _defect.setSelected(cb.isChecked());
                        if(cb.isChecked()){
                            // holder.info.setVisibility(View.VISIBLE);
                            // holder.asterisk.setVisibility(View.VISIBLE);

                        } else {
                            // holder.info.setVisibility(View.INVISIBLE);
                            // holder.asterisk.setVisibility(View.INVISIBLE);
                        }
                    }
                });
            }
            else {
                holder = (ViewHolder) convertView.getTag();
            }

            Defects defect = defectsList.get(position);

            //holder.code.setText(" (" + defect.getCode() + ")");
            holder.name.setText(defect.getName());
            holder.name.setChecked(defect.isSelected());
            holder.name.setTag(defect);

            return convertView;
        }
    }
}