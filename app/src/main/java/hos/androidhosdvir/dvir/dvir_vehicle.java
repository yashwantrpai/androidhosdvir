package hos.androidhosdvir.dvir;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.ArrayList;

import hos.androidhosdvir.DVIR;
import hos.androidhosdvir.Defects;
import hos.androidhosdvir.HomeActivity;
import hos.androidhosdvir.R;

public class dvir_vehicle extends Fragment {

    public dvir_vehicle() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public static View rootView;
    public static Fragment defectsFragment;
    public static EditText vehicle_number, trailer_number, miles, hours;
    public static LinearLayout add_remove_vehicle_defects, add_remove_trailer_defects, defects_vehicle, defects_trailer;
    public static Boolean isVehicle = true;
    public static RadioGroup inspection_type;
    public static RadioButton pre_inspection, post_inspection;
    public static Boolean valuesEdited = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_dvir_vehicle, container, false);
        Button vehicle_btn = (Button) rootView.findViewById(R.id.vehicle_dvir_btn);
        vehicle_btn.setSelected(true);
        defectsFragment = new defects();
        defects_vehicle = (LinearLayout) rootView.findViewById(R.id.defects_vehicle);
        defects_trailer = (LinearLayout) rootView.findViewById(R.id.defects_trailer);
        vehicle_number = (EditText) rootView.findViewById(R.id.vehicle_number);
        trailer_number = (EditText) rootView.findViewById(R.id.trailer_number);
        miles = (EditText) rootView.findViewById(R.id.miles);
        hours = (EditText) rootView.findViewById(R.id.hours);
        inspection_type = (RadioGroup) rootView.findViewById(R.id.inspection_type);
        pre_inspection = (RadioButton) rootView.findViewById(R.id.pre_inspection);
        post_inspection = (RadioButton) rootView.findViewById(R.id.post_inspection);
        valuesEdited = false;
        HomeActivity.cur_fragment = "dvir_vehicle";
        // retain this fragment
        setRetainInstance(true);

        ImageView back = (ImageView) rootView.findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DVIR.temp_vehicle_no = "";
                DVIR.temp_trailer_no = "";
                DVIR.temp_miles = "";
                DVIR.temp_hours = "";
                DVIR.temp_inspection_type = 2;
                DVIR.temp_vehicle_defects = new JSONArray();
                DVIR.temp_trailer_defects = new JSONArray();
                DVIR.vehicleDefects = new ArrayList<Defects>();
                DVIR.trailerDefects = new ArrayList<Defects>();

                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.main_content, new DVIR(), "DVIR");
                transaction.addToBackStack(null);
                transaction.commitAllowingStateLoss();
            }
        });

        Button general_btn = (Button) rootView.findViewById(R.id.general_dvir_btn);
        general_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!valuesEdited) {
                    DVIR.temp_vehicle_no = vehicle_number.getText().toString();
                    DVIR.temp_trailer_no = trailer_number.getText().toString();
                    if (inspection_type.getCheckedRadioButtonId() == R.id.pre_inspection) {
                        DVIR.temp_inspection_type = 0;
                    } else if (inspection_type.getCheckedRadioButtonId() == R.id.post_inspection) {
                        DVIR.temp_inspection_type = 1;
                    } else {
                        DVIR.temp_inspection_type = 2;
                    }
                    DVIR.temp_miles = miles.getText().toString();
                    DVIR.temp_hours = hours.getText().toString();

                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction transaction = fragmentManager.beginTransaction();
                    transaction.replace(R.id.main_content, new dvir_general(), "dvir_general");
                    transaction.addToBackStack(null);
                    transaction.commitAllowingStateLoss();
                } else {
                    Toast.makeText(getContext(), "Please save your changes to proceed", Toast.LENGTH_SHORT).show();
                }
            }
        });

        Button sign_btn = (Button) rootView.findViewById(R.id.sign_dvir_btn);
        sign_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!valuesEdited){
                    DVIR.temp_trailer_no = trailer_number.getText().toString();
                    if(inspection_type.getCheckedRadioButtonId() == R.id.pre_inspection) {
                        DVIR.temp_inspection_type = 0;
                    } else if(inspection_type.getCheckedRadioButtonId() == R.id.post_inspection){
                        DVIR.temp_inspection_type = 1;
                    } else {
                        DVIR.temp_inspection_type = 2;
                        DVIR.temp_vehicle_no = vehicle_number.getText().toString();
                    }
                    DVIR.temp_miles = miles.getText().toString();
                    DVIR.temp_hours = hours.getText().toString();

                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction transaction = fragmentManager.beginTransaction();
                    transaction.replace(R.id.main_content, new dvir_sign(), "dvir_sign");
                    transaction.addToBackStack(null);
                    transaction.commitAllowingStateLoss();
                } else {
                    Toast.makeText(getContext(), "Please save your changes to proceed", Toast.LENGTH_SHORT).show();
                }
            }
        });

        inspection_type.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                valuesEdited = true;
            }
        });

        add_remove_vehicle_defects = (LinearLayout) rootView.findViewById(R.id.add_remove_vehicle_defects);
        add_remove_vehicle_defects.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DVIR.temp_vehicle_no = vehicle_number.getText().toString();
                DVIR.temp_trailer_no = trailer_number.getText().toString();
                if(inspection_type.getCheckedRadioButtonId() == R.id.pre_inspection) {
                    DVIR.temp_inspection_type = 0;
                } else if(inspection_type.getCheckedRadioButtonId() == R.id.post_inspection){
                    DVIR.temp_inspection_type = 1;
                } else {
                    DVIR.temp_inspection_type = 2;
                }
                DVIR.temp_miles = miles.getText().toString();
                DVIR.temp_hours = hours.getText().toString();

                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                isVehicle = true;
                transaction.replace(R.id.main_content, defectsFragment, "defects");
                transaction.addToBackStack(null);
                transaction.commitAllowingStateLoss();
            }
        });

        add_remove_trailer_defects = (LinearLayout) rootView.findViewById(R.id.add_remove_trailer_defects);
        add_remove_trailer_defects.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DVIR.temp_vehicle_no = vehicle_number.getText().toString();
                DVIR.temp_trailer_no = trailer_number.getText().toString();
                if(inspection_type.getCheckedRadioButtonId() == R.id.pre_inspection) {
                    DVIR.temp_inspection_type = 0;
                } else if(inspection_type.getCheckedRadioButtonId() == R.id.post_inspection){
                    DVIR.temp_inspection_type = 1;
                } else {
                    DVIR.temp_inspection_type = 2;
                }
                DVIR.temp_miles = miles.getText().toString();
                DVIR.temp_hours = hours.getText().toString();

                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                isVehicle = false;
                transaction.replace(R.id.main_content, defectsFragment, "defects");
                transaction.addToBackStack(null);
                transaction.commitAllowingStateLoss();
            }
        });

        Button save_vehicle = (Button) rootView.findViewById(R.id.save_vehicle);
        save_vehicle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!vehicle_number.getText().toString().matches("") && !miles.getText().toString().matches("") && !hours.getText().toString().matches("")) {
                    DVIR.temp_vehicle_no = vehicle_number.getText().toString();
                    DVIR.temp_trailer_no = trailer_number.getText().toString();
                    if (inspection_type.getCheckedRadioButtonId() == R.id.pre_inspection) {
                        DVIR.temp_inspection_type = 0;
                    } else if (inspection_type.getCheckedRadioButtonId() == R.id.post_inspection) {
                        DVIR.temp_inspection_type = 1;
                    } else {
                        DVIR.temp_inspection_type = 2;
                    }
                    DVIR.temp_miles = miles.getText().toString();
                    DVIR.temp_hours = hours.getText().toString();
                    valuesEdited = false;
                    Toast.makeText(getContext(), "Changes saved", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getContext(), "Please enter valid data for fields marked as important(*)", Toast.LENGTH_LONG).show();
                }
                //}
            }
        });

        LinearLayout no_defect_layout = new LinearLayout(getContext());
        no_defect_layout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));

        ImageView imageView = new ImageView(getContext());
        imageView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT, 0.15f));
        imageView.setImageResource(R.drawable.defect);

        TextView textView = new TextView(getContext());
        imageView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT));
        textView.setText("No Defects");
        textView.setGravity(Gravity.CENTER);
        no_defect_layout.addView(imageView);
        no_defect_layout.addView(textView);

        vehicle_number.setText(DVIR.temp_vehicle_no);
        trailer_number.setText(DVIR.temp_trailer_no);
        miles.setText(DVIR.temp_miles);
        hours.setText(DVIR.temp_hours);
        if(DVIR.temp_inspection_type == 0){
            pre_inspection.setChecked(true);
            post_inspection.setChecked(false);
        } else if(DVIR.temp_inspection_type == 1){
            pre_inspection.setChecked(false);
            post_inspection.setChecked(true);
        } else {
            pre_inspection.setChecked(false);
            post_inspection.setChecked(false);
        }

        if(HomeActivity.isEdit) {
            String defects_array[] = getResources().getStringArray(R.array.defects_array);
            Defects _defects;

            // Check all existing defects of both trailer and vehicle for the DVIR in edit
            try {
            // vehicleDefects
                if( !DVIR.isVehicleEdited && DVIR.vehicleDefects.size() == 0 ) {
                    for (int i = 0; i < defects_array.length; i++) {
                        Boolean check = false;
                        String str1 = defects_array[i];
                        for (int j = 0; j < DVIR.temp_vehicle_defects.length(); j++) {
                            JSONObject defect_json = DVIR.temp_vehicle_defects.getJSONObject(j);
                            String str2 = defect_json.get("defects").toString();
                            if (equalStrings(str1, str2)) {
                                check = true;
                                break;
                            }
                        }
                        _defects = new Defects(String.valueOf(i), defects_array[i], "", check);
                        DVIR.vehicleDefects.add(_defects);
                    }
                } else {

                }

            // trailerDefects
                if( !DVIR.isTrailerEdited && DVIR.trailerDefects.size() == 0 ) {
                    for (int i = 0; i < DVIR.trailer_defect_ind.length; i++) {
                        Boolean check = false;
                        String str1 = defects_array[DVIR.trailer_defect_ind[i]];
                        for (int j = 0; j < DVIR.temp_trailer_defects.length(); j++) {
                            JSONObject defect_json = DVIR.temp_trailer_defects.getJSONObject(j);
                            String str2 = defect_json.get("defects").toString();
                            if (equalStrings(str1, str2)) {
                                check = true;
                                break;
                            }
                        }

                        _defects = new Defects(String.valueOf(DVIR.trailer_defect_ind[i]), defects_array[DVIR.trailer_defect_ind[i]], "", check);
                        DVIR.trailerDefects.add(_defects);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

            if (DVIR.vehicleDefects != null && !DVIR.vehicleDefects.isEmpty() && DVIR.vehicleDefects.size() != 0) {
                if (defects_vehicle.getChildCount() > 0) {
                    defects_vehicle.removeAllViews();
                }

                DVIR.temp_vehicle_defects = new JSONArray();
                for (int i = 0; i < DVIR.vehicleDefects.size(); i++) {
                    if (DVIR.vehicleDefects.get(i).isSelected()) {
                        LinearLayout layout = new LinearLayout(getContext());
                        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                        layout.setLayoutParams(params);
                        layout.setOrientation(LinearLayout.HORIZONTAL);

                        ImageView defect_img = new ImageView(getContext());
                        defect_img.setLayoutParams(new LinearLayout.LayoutParams(60, LinearLayout.LayoutParams.MATCH_PARENT));
                        defect_img.setPadding(8, 10, 8, 10);
                        try {
                            if (android.os.Build.VERSION.SDK_INT >= 21) {
                                defect_img.setImageDrawable(getResources().getDrawable(R.drawable.defect, getContext().getTheme()));
                            } else {
                                defect_img.setImageDrawable(getResources().getDrawable(R.drawable.defect));
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }

                        layout.addView(defect_img);

                        TextView defect_text = new TextView(getContext());
                        defect_text.setTextAppearance(getContext(), android.R.style.TextAppearance_DeviceDefault_Medium);
                        defect_text.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                        defect_text.setMinimumHeight(100);
                        defect_text.setTextColor(0xFF000000);
                        defect_text.setTypeface(Typeface.SERIF);
                        defect_text.setGravity(Gravity.CENTER);
                        defect_text.setPadding(20, 0, 0, 0);
                        //table_duration_text.setTextSize(11);
                        defect_text.setText(DVIR.vehicleDefects.get(i).getName().toString());
                        layout.addView(defect_text);

                        defects_vehicle.addView(layout);
                        DVIR.temp_vehicle_defects.put(DVIR.vehicleDefects.get(i).getName().toString());
                    }
                }
            } else {
                if (defects_vehicle.getChildCount() > 0) {
                    defects_vehicle.removeAllViews();
                }
            }

        if (DVIR.trailerDefects != null && !DVIR.trailerDefects.isEmpty() && DVIR.trailerDefects.size() != 0) {
            if (defects_trailer.getChildCount() > 0) {
                defects_trailer.removeAllViews();
            }

            DVIR.temp_trailer_defects = new JSONArray();
            for (int i = 0; i < DVIR.trailerDefects.size(); i++) {
                if (DVIR.trailerDefects.get(i).isSelected()) {
                    LinearLayout layout = new LinearLayout(getContext());
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    layout.setLayoutParams(params);
                    //layout.setMinimumHeight(100);
                    layout.setOrientation(LinearLayout.HORIZONTAL);

                    ImageView defect_img = new ImageView(getContext());
                    defect_img.setLayoutParams(new LinearLayout.LayoutParams(60, LinearLayout.LayoutParams.MATCH_PARENT));
                    defect_img.setPadding(8, 10, 8, 10);
                    try {
                        if(android.os.Build.VERSION.SDK_INT >= 21){
                            defect_img.setImageDrawable(getResources().getDrawable(R.drawable.defect, getContext().getTheme()));
                        } else {
                            defect_img.setImageDrawable(getResources().getDrawable(R.drawable.defect));
                        }
                    } catch (Exception e){
                        e.printStackTrace();
                    }
                    layout.addView(defect_img);

                    TextView defect_text = new TextView(getContext());
                    defect_text.setTextAppearance(getContext(), android.R.style.TextAppearance_DeviceDefault_Medium);
                    defect_text.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                    defect_text.setMinimumHeight(100);
                    defect_text.setTextColor(0xFF000000);
                    defect_text.setTypeface(Typeface.SERIF);
                    defect_text.setGravity(Gravity.CENTER);
                    defect_text.setPadding(20, 0, 0, 0);
                    //table_duration_text.setTextSize(11);
                    defect_text.setText(DVIR.trailerDefects.get(i).getName().toString());
                    layout.addView(defect_text);

                    defects_trailer.addView(layout);
                    DVIR.temp_trailer_defects.put(DVIR.trailerDefects.get(i).getName().toString());
                }
            }
        } else {
            if (defects_trailer.getChildCount() > 0) {
                defects_trailer.removeAllViews();
            }
        }

        return rootView;
    }

    @Override
    public void onDetach() {
        super.onDetach();

        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);

        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    public boolean equalStrings(String sb1, String sb2) {
        int len = sb1.length();
        if (len != sb2.length()) {
            return false;
        }
        for (int i = 0; i < len; i++) {
            if (sb1.charAt(i) != sb2.charAt(i)) {
                return false;
            }
        }
        return true;
    }
}
