package hos.androidhosdvir.dvir;

import android.Manifest;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TimePicker;
import android.widget.Toast;
import java.io.IOException;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import hos.androidhosdvir.DVIR;
import hos.androidhosdvir.HomeActivity;
import hos.androidhosdvir.Login;
import hos.androidhosdvir.R;
import hos.androidhosdvir.util.Util;
import hos.androidhosdvir.util.UtilDate;

/**
 * A simple {@link Fragment} subclass.
 */
public class dvir_general extends Fragment {

    public dvir_general() {
        // Required empty public constructor
    }

    public static View rootView;
    public static EditText time_text, location_text, carrier_text, odometer_text;
    TimePickerDialog timePickerDialog;
    ImageView clock_btn;
    int hour, min;
    Context app_context, activity_context;
    public static ConnectivityManager connectivityManager;
    public static String[] perms = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
    public static int code = 200;
    public static Location mLastLocation;
    public static double latitude = 0.0, longitude = 0.0;
    public static Boolean valuesEdited = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_dvir_general, container, false);
        Button general_btn = (Button) rootView.findViewById(R.id.general_dvir_btn);
        general_btn.setSelected(true);
        app_context = this.getContext();
        activity_context = this.getActivity();
        connectivityManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        // retain this fragment
        setRetainInstance(true);
        clock_btn = (ImageView) rootView.findViewById(R.id.clock_btn);
        time_text = (EditText) rootView.findViewById(R.id.time);
        carrier_text = (EditText) rootView.findViewById(R.id.carrier);
        location_text = (EditText) rootView.findViewById(R.id.location);
        odometer_text = (EditText) rootView.findViewById(R.id.odometer);

        valuesEdited = false;
        HomeActivity.cur_fragment = "dvir_general";

        time_text.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                valuesEdited = true;
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        carrier_text.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                valuesEdited = true;
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        location_text.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                valuesEdited = true;
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        odometer_text.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                valuesEdited = true;
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        ImageView back = (ImageView) rootView.findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DVIR.temp_time = "";
                DVIR.temp_location = "";
                DVIR.temp_carrier = "";
                DVIR.temp_odometer = "";

                if(!getActivity().isFinishing()) {
                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction transaction = fragmentManager.beginTransaction();
                    transaction.replace(R.id.main_content, new DVIR(), "DVIR");
                    transaction.addToBackStack(null);
                    transaction.commitAllowingStateLoss();
                }
            }
        });

        Button vehicle_btn = (Button) rootView.findViewById(R.id.vehicle_dvir_btn);
        vehicle_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!valuesEdited) {
                    if(!getActivity().isFinishing()) {
                        FragmentManager fragmentManager = getFragmentManager();
                        FragmentTransaction transaction = fragmentManager.beginTransaction();
                        transaction.replace(R.id.main_content, new dvir_vehicle(), "dvir_vehicle");
                        transaction.addToBackStack(null);
                        transaction.commitAllowingStateLoss();
                    }
                } else {
                    Toast.makeText(getContext(), "Please save your changes to proceed", Toast.LENGTH_SHORT).show();
                }
            }
        });

        Button sign_btn = (Button) rootView.findViewById(R.id.sign_dvir_btn);
        sign_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!valuesEdited) {
                    if(!getActivity().isFinishing()) {
                        FragmentManager fragmentManager = getFragmentManager();
                        FragmentTransaction transaction = fragmentManager.beginTransaction();
                        transaction.replace(R.id.main_content, new dvir_sign(), "dvir_sign");
                        transaction.addToBackStack(null);
                        transaction.commitAllowingStateLoss();
                    }
                } else {
                    Toast.makeText(getContext(), "Please save your changes to proceed", Toast.LENGTH_SHORT).show();
                }
            }
        });

        Button save_general = (Button) rootView.findViewById(R.id.save_general);
        save_general.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!time_text.getText().toString().matches("") && !location_text.getText().toString().matches("") && !carrier_text.getText().toString().matches("") && !odometer_text.getText().toString().matches("")){
                    DVIR.temp_time = time_text.getText().toString();
                    DVIR.temp_location = location_text.getText().toString();
                    DVIR.temp_carrier = carrier_text.getText().toString();
                    DVIR.temp_odometer = odometer_text.getText().toString();
                    valuesEdited = false;
                    Toast.makeText(getContext(), "Changes saved", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getContext(), "Please enter valid data for fields marked as important(*)", Toast.LENGTH_LONG).show();
                }
            }
        });

        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        if (HomeActivity.isEdit) {
            time_text.setText(DVIR.temp_time);
            location_text.setText(DVIR.temp_location);
            carrier_text.setText(DVIR.temp_carrier);
            odometer_text.setText(DVIR.temp_odometer);
        } else {
            if(DVIR.temp_time.equals("") && Login.cur_date.equals(Util.getTodayDate())){
                time_text.setText(UtilDate.getSlotNumberForLoggedInUser().get("hour")+":"+UtilDate.getSlotNumberForLoggedInUser().get("min")+":"+UtilDate.getSlotNumberForLoggedInUser().get("sec"));
            }else{
                time_text.setText(DVIR.temp_time);
            }

            if(DVIR.temp_location.equals("")){
                getLocation();
                getLocationText();
            }else{
                location_text.setText(DVIR.temp_location);
            }

            if(!DVIR.temp_odometer.equals("")){
                odometer_text.setText(DVIR.temp_odometer);
            }

            if(!DVIR.temp_carrier.equals("")){
                carrier_text.setText(DVIR.temp_carrier);
            }
        }

        clock_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                timePickerDialog = new TimePickerDialog(activity_context, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int hourOfDay, int minute) {
                        hour = hourOfDay;
                        min = minute;

                        time_text.setText(hour + ":" + min);
                    }
                }, hour, min, true);
                timePickerDialog.show();
            }
        });

        return rootView;
    }

    @Override
    public void onDetach() {
        super.onDetach();

        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);

        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    public void getLocation(){
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            requestPermissions(perms, code);
        } else {

        }
    }

    public void getLocationText(){
        if(mLastLocation != null){
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
                getLocationAddress getLocationAddress = new getLocationAddress();
                getLocationAddress.execute();
            } else {
                Toast.makeText(app_context, "Please check your internet connectivity", Toast.LENGTH_SHORT).show();
            }
        } else {
            location_text.setText("");
            //Toast.makeText(app_context, "Location currently not available", Toast.LENGTH_SHORT).show();
        }
    }

    private class getLocationAddress extends AsyncTask<String, Void, List<Address>> implements DialogInterface.OnCancelListener {
        private ProgressDialog pdia;

        /* @Override
        protected void onPreExecute(){
            super.onPreExecute();
            pdia = new ProgressDialog(activity_context);
            pdia.setMessage("Fetching current location..");
            pdia.setCancelable(false);
            pdia.show();
        } */

        @Override
        protected List<Address> doInBackground(String... params) {
            Geocoder geocoder;
            List<Address> addresses = null;
            geocoder = new Geocoder(app_context, Locale.getDefault());

            try {
                addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents_myfiles it recommended 1 to 5
            } catch (IOException e) {
                e.printStackTrace();
            }
            return addresses;
        }

        @Override
        protected void onPostExecute(List<Address> addresses){
            //if (pdia != null && pdia.isShowing()) {
                //pdia.hide();
                //pdia.dismiss();
            //}
            if (addresses != null && addresses.size() > 0 && !addresses.isEmpty()) {
                String addr = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                String city = addresses.get(0).getAddressLine(1);
                String state = addresses.get(0).getAdminArea();

                String display = "";
                if (addr != null && !addr.isEmpty()) {
                    display += (addr + " ");
                }
                if (city != null && !city.isEmpty()) {
                    display += (city + " ");
                }
                if (state != null && !state.isEmpty()) {
                    // display += (state + " ");
                }
                location_text.setText(display);
                int height_in_pixels = location_text.getLineCount() * location_text.getLineHeight(); //approx height text
                location_text.setHeight(height_in_pixels);
            } else {
                location_text.setText("");
                Toast.makeText(app_context, "Location not found", Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onCancel(DialogInterface dialog) {
            cancel(true);
        }
    }
}
