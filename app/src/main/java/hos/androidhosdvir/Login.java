package hos.androidhosdvir;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.okhttp.OkHttpClient;

import org.json.JSONObject;

import java.net.CookieHandler;
import java.util.ArrayList;

import hos.androidhosdvir.db.DBConnection;
import hos.androidhosdvir.db.DBHelper;
import hos.androidhosdvir.util.Util;
import hos.androidhosdvir.web.CustomHttpClient;
import hos.androidhosdvir.web.WebkitCookieManagerProxy;

public class Login extends Activity {
    public static EditText username, password;
    private static final String TAG = "LoginActivity";
    //GetRequest asyncTask = new GetRequest();
    public static WebkitCookieManagerProxy coreCookieManager;
    ConnectivityManager connectivityManager;
    public static Context context;
    Button login;
    String response = "";
    public static String cur_date;
    TextView forgot_password;
    JSONObject loginObject;
    public static OkHttpClient OkHTTPClient;
    public static DBConnection db;

    public static OkHttpClient getClient() {
        if(OkHTTPClient == null) {
            OkHTTPClient = new OkHttpClient();

            // initialize WebkitCookieManagerProxy, set Cookie Policy, create android.webkit.webkitCookieManager
            coreCookieManager = new WebkitCookieManagerProxy(null, java.net.CookiePolicy.ACCEPT_ALL);
            CookieHandler.setDefault(coreCookieManager);
            OkHTTPClient.setCookieHandler(coreCookieManager);
        }
        return OkHTTPClient;
    }

    @TargetApi(Build.VERSION_CODES.M)
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        //asyncTask.listener = this;
        context = this;
        db = new DBConnection(context);

        Window window = this.getWindow();

        // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        // finally change the color
        window.setStatusBarColor(ContextCompat.getColor(context, R.color.colorNotification));

        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);

        cur_date = Util.getTodayDate();

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_NETWORK_STATE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_NETWORK_STATE}, 200);
        }

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.INTERNET}, 201);
        }

        connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        if(DBHelper.getLoggedInUserId() != 0) {     // Someone is logged in
            Intent i = new Intent(Login.this, HomeActivity.class);
            startActivity(i);
        }

        login = (Button) findViewById(R.id.login);
        login.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                /* Intent i = new Intent(Login.this, HomeActivity.class);
                startActivity(i);
                DBHelper.login(username.getText().toString().trim(), password.getText().toString().trim()); */
                NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
                if(activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
                    // +"?username=" + email.getText().toString().trim() + "&password=" + passcode.getText().toString().trim()
                    login();
                }else{
                    Toast.makeText(Login.this, "Please check your internet connectivity!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 200: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {
                    if(!isFinishing()){
                        AlertDialog permissionDeniedAlert = new AlertDialog.Builder(Login.this).create();
                        permissionDeniedAlert.setTitle("Network State Permission Denied");
                        permissionDeniedAlert.setMessage("Some features may not work correctly without network state permission");
                        permissionDeniedAlert.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                        permissionDeniedAlert.show();
                    }
                }
                return;
            }

            case 201: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {
                    if(!isFinishing()){
                        AlertDialog permissionDeniedAlert = new AlertDialog.Builder(Login.this).create();
                        permissionDeniedAlert.setTitle("Internet Permission Denied");
                        permissionDeniedAlert.setMessage("Some features may not work correctly without internet permission");
                        permissionDeniedAlert.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                        permissionDeniedAlert.show();
                    }
                }
                return;
            }
        }
    }

    public void login(){
        SharedPreferences pref;
        final String LoginServer = "http://hos-server.com/client/login_dvir.php";

        ArrayList params = new ArrayList();
        Pair pair = new Pair("operation", "login");
        params.add(pair);
        pair = new Pair("username", username.getText().toString().trim());
        params.add(pair);
        pair = new Pair("password", password.getText().toString().trim());
        params.add(pair);

        String postString = Util.createPostString(params);
        validateUserTask task = new validateUserTask();
        task.execute(LoginServer, postString);
    }

    private class validateUserTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... url) {
            // TODO Auto-generated method stub
            String res = null;
            try {
                response = CustomHttpClient.executeHttpPost(url[0], url[1]);
                res=response.toString();
                //res= res.replaceAll("\\s+","");
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            return res;
        }//close doInBackground

        @Override
        protected void onPostExecute(String result) {
            response = result;
            Boolean responsefailed = false;

            if(!HomeActivity.responseError) {
                if (result != null && result != "" && !response.isEmpty()) {
                    String result_parts[] = result.split("###");

                    if (result_parts.length >= 2) {
                        if (result_parts[0].contains("login")) {
                            if (result_parts[1].contains("success")) {
                                DBHelper.login(username.getText().toString().trim(), password.getText().toString().trim());
                                Intent i = new Intent(Login.this, HomeActivity.class);
                                startActivity(i);
                                Toast.makeText(context, "Login successful", Toast.LENGTH_SHORT).show();
                            } else if (result_parts[1].contains("failed")) {
                                responsefailed = true;
                            } else {
                                responsefailed = true;
                            }
                        } else {
                            responsefailed = true;
                        }
                    } else {
                        responsefailed = true;
                    }
                } else {
                    responsefailed = true;
                }

                if (responsefailed && !HomeActivity.responseError) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setTitle("Login failed");
                    builder.setMessage("Username/Password wrong")
                            .setCancelable(false)
                            .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Login failed");
                builder.setMessage("Server response error")
                        .setCancelable(false)
                        .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
            }
        }//close onPostExecute
    }// close validateUserTask

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }
}

