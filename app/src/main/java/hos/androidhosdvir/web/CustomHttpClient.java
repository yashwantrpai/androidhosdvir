package hos.androidhosdvir.web;

import android.util.Log;

import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.io.InputStream;

import hos.androidhosdvir.HomeActivity;
import hos.androidhosdvir.Login;

public class CustomHttpClient {
    /**
     * Single instance of our HttpClient
     */
    static OkHttpClient client = new OkHttpClient();
    static String ret;
    public static int respStatus;
    private static final String TAG = "CustomHttpClient";

    /**
     * Get our single instance of our HttpClient object
     * @return an HttpClient object with connection parameters set
     */
    /**
     * Performs an HTTP Post request to the specified url with the
     * specified parameters.
     *
     * @param url The web address to post the request to
     * @return The result of the request
     * @throws Exception
     */

    // Send a POST request
    public static String executeHttpPost(String url, String postString) throws Exception {
        Log.d("POST Request", url);
        InputStream in = null;
        Request request = null;

        try{
            RequestBody body = new FormEncodingBuilder()
                    .add("payload", postString)
                    .build();
            request = new Request.Builder().url(url).post(body).build();

            Response response = Login.getClient().newCall(request).execute();
            ret = response.body().string();

            if(respStatus == 403 || respStatus == 404){
                HomeActivity.responseError = true;
            } else {
                HomeActivity.responseError = false;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ret;
    }

    // Send a GET request
    public static String executeHttpPost(String url) throws Exception {
        Log.d("GET request", url);
        try {
            Request request = new Request.Builder().url(url).build();
            Response response = Login.getClient().newCall(request).execute();
            respStatus = response.code();
            ret = response.body().string();
            if(respStatus == 403 || respStatus == 404){
                HomeActivity.responseError = true;
            } else {
                HomeActivity.responseError = false;
            }
            // Log.d(url, ret);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return ret;
    }
}

