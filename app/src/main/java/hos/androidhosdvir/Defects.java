package hos.androidhosdvir;

public class Defects
{
    String code = null;
    String name = null;
    String info = null;
    boolean selected = false;

    public Defects(String code, String name, String info, boolean selected)
    {
        super();
        this.code = code;
        this.name = name;
        this.info = info;
        this.selected = selected;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getInfo(){
        return info;
    }

    public void setInfo(String info){
        this.info = info;
    }

    public boolean isSelected()
    {
        return selected;
    }

    public void setSelected(boolean selected)
    {
        this.selected = selected;
    }
}
