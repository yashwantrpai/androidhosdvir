package hos.androidhosdvir;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import hos.androidhosdvir.db.DBConnection;
import hos.androidhosdvir.db.DBHelper;
import hos.androidhosdvir.dvir.defects;
import hos.androidhosdvir.dvir.dvir_general;
import hos.androidhosdvir.dvir.dvir_sign;
import hos.androidhosdvir.dvir.dvir_vehicle;
import hos.androidhosdvir.settings.Settings;
import hos.androidhosdvir.util.Util;
import hos.androidhosdvir.util.UtilDate;

public class HomeActivity extends AppCompatActivity {

    public static int person = -1;
    public static boolean isEdit = false;
    public static String cur_fragment;
    public static Context context;
    public static boolean responseError = false;
    public static String mechanicSign = "", driverSign = "";
    public static DBConnection db;

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        context = this;

        db = new DBConnection(context);

        Window window = this.getWindow();

        // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        // finally change the color
        window.setStatusBarColor(ContextCompat.getColor(context, R.color.colorNotification));

        LinearLayout mainContent = (LinearLayout) findViewById(R.id.main_content);

        getCurrentFragment();

    }

    @Override
    protected void onResume() {
        super.onResume();
        //getCurrentFragment();
    }

    public void getCurrentFragment(){
        Fragment fragment = new DVIR();
        if(HomeActivity.cur_fragment == null || HomeActivity.cur_fragment.equals("") || HomeActivity.cur_fragment.equals("DVIR")){
            fragment = new DVIR();
            HomeActivity.cur_fragment = "DVIR";
        }else if(HomeActivity.cur_fragment.equals("defects")) {
            fragment = new defects();
        }else if(HomeActivity.cur_fragment.equals("dvir_general")){
            fragment = new dvir_general();
        }else if(HomeActivity.cur_fragment.equals("dvir_sign")) {
            fragment = new dvir_sign();
        }else if(HomeActivity.cur_fragment.equals("dvir_vehicle")){
            fragment = new dvir_vehicle();
        }else if(HomeActivity.cur_fragment.equals("settings")){
            fragment = new Settings();
        }else{
            fragment = new DVIR();
        }
        if(fragment != null && !this.isFinishing()) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.replace(R.id.main_content, fragment, HomeActivity.cur_fragment);
            transaction.addToBackStack(null);
            try {
                transaction.commitAllowingStateLoss();
            }catch (Exception e){
                e.printStackTrace();
                getCurrentFragment();
            }
        } else {
            getCurrentFragment();
        }
    }
}
