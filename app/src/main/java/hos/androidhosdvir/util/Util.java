package hos.androidhosdvir.util;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.util.Log;
import android.util.Pair;
import android.widget.TextView;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.TimeZone;
import java.util.List;

public class Util {

    public static final String MAP_START_DATE = "startDate";
    public static final String MAP_START_DATE_START_SLOT = "startDateStartSlot";
    public static final String MAP_START_DATE_END_SLOT = "startDateEndSlot";
    public static final String MAP_END_DATE = "endDate";
    public static final String MAP_END_DATE_START_SLOT = "endDateStartSlot";
    public static final String MAP_END_DATE_END_SLOT = "endDateEndSlot";

    public static final String CURSOR_LOG_GRAPH_USER_ID = "userid";
    public static final String CURSOR_LOG_GRAPH_DRIVER_STATUS = "driverstatus";
    public static final String CURSOR_LOG_GRAPH_VEHICLE_STATUS = "vehiclestatus";
    public static final String CURSOR_LOG_GRAPH_NOTES = "annotation";
    public static final String CURSOR_LOG_GRAPH_SLOT = "slot";
    public static final String CURSOR_LOG_GRAPH_DATE = "date";
    public static final String CURSOR_LOG_LOCATION = "location";

    public static String correctData(String str) {
        if (str != null && !str.equals("")) {
            return str;
        }
        return "";
    }

    public static int correctData(int value) {
        return value;
    }

    public static String createPostString(ArrayList<Pair> params){
        String postString = "";

        // "operation::login###username::"+params.get("username")+"###password::"+params.get("password")+" ###mobileidentifier::"+params.get("mobileidentifier")+"###useragent::"+params.get("useragent")+"###pushidentifier::"+params.get("pushidentifier")
        for(int i = 0; i < params.size(); i++) {
            postString += (params.get(i).first + "::" +params.get(i).second);
            if(i < (params.size() - 1)){
                postString += "###";
            }
        }

        return postString;
    }

    public static Map<String, String> getQueryConstructMapForADay(String date, String timediff) {
        //String start = UtilDate.getUTCTime(startDateTime, timediff, true);
        //String end = UtilDate.getUTCTime(endDateTime, timediff, false);

        //String date = "11/10/2017";
        String calculatedStartDateTime = "";
        String calculatedEndDateTime = "";

        Map<String, String> queryMap = new HashMap<>();


        try {
            String dateTimeStart = date + " 00:00:00";
            String dateTimeEnd = date + " 24:00:00";
            String dateTimeCurrentMoment = UtilDate.getCurrrentUTCTime();

            Calendar calStart = Calendar.getInstance();
            Calendar calEnd = Calendar.getInstance();
            Calendar calCurrentMoment = Calendar.getInstance();

            SimpleDateFormat sourceStartFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm");
            //sourceFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            sourceStartFormat.setTimeZone(TimeZone.getTimeZone(timediff));
            Date startParsed = sourceStartFormat.parse(dateTimeStart);
            calStart.setTime(startParsed);
            calStart.add(Calendar.MINUTE, 1);

            SimpleDateFormat sourceEndFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm");
            //sourceFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            sourceEndFormat.setTimeZone(TimeZone.getTimeZone(timediff));
            Date endParsed = sourceEndFormat.parse(dateTimeEnd);
            calEnd.setTime(endParsed);
            calEnd.add(Calendar.MINUTE, -1);

            SimpleDateFormat sourceCurrentMomentFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm");
            //sourceFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            sourceCurrentMomentFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date currentMomentParsed = sourceCurrentMomentFormat.parse(dateTimeCurrentMoment);
            calCurrentMoment.setTime(currentMomentParsed);

            TimeZone tz = TimeZone.getTimeZone("UTC");

            SimpleDateFormat destFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
            destFormat.setTimeZone(tz);

            calculatedStartDateTime = destFormat.format(calStart.getTime());

            String endResult = destFormat.format(calEnd.getTime());

            String currentMomentResult = destFormat.format(calCurrentMoment.getTime());

            if (calCurrentMoment.before(calEnd)) {
                //System.out.println("use current moment");
                calculatedEndDateTime = currentMomentResult;
            } else {
                System.out.println("use end date");
                calculatedEndDateTime = endResult;
            }

            Map<String, String> startMap = UtilDate.getDateHourMin(calculatedStartDateTime);
            Map<String, String> endMap = UtilDate.getDateHourMin(calculatedEndDateTime);

            //System.out.println("Start Result: " + startResult);
            //System.out.println("End Result: " + endResult);
            //System.out.println("Current moment Result: " + currentMomentResult);

            String startDateStartSlot = String.valueOf(UtilDate.getSlot(startMap.get("hour"), startMap.get("min")));
            String endDateEndSlot = String.valueOf(UtilDate.getSlot(endMap.get("hour"), endMap.get("min")));

            queryMap.put(MAP_START_DATE, startMap.get("date"));
            queryMap.put(MAP_START_DATE_START_SLOT, startDateStartSlot);
            queryMap.put(MAP_START_DATE_END_SLOT, "96");
            queryMap.put(MAP_END_DATE, endMap.get("date"));
            queryMap.put(MAP_END_DATE_START_SLOT, "1");
            queryMap.put(MAP_END_DATE_END_SLOT, endDateEndSlot);


        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return queryMap;
    }

    public static List<String> getSQLQuery(int userid, String date, Map<String, String> queryMap, boolean ui) {
        List<String> sqlQueryList = new ArrayList<>();
        StringBuilder sqlQueryCommon = null;
        StringBuilder sqlQueryOne = null;
        StringBuilder sqlQueryTwo = null;
        Log.i("map", "Map start date: " + queryMap.get(Util.MAP_START_DATE));
        Log.i("map", "Map end date: " + queryMap.get(Util.MAP_END_DATE));

        if (ui) {
            if (queryMap.get(Util.MAP_START_DATE).equalsIgnoreCase(queryMap.get(Util.MAP_END_DATE))) {
                //one query
                sqlQueryCommon = new StringBuilder();
                sqlQueryCommon.append("SELECT userid, date, slot, driverstatus, vehiclestatus, distance FROM loggraph WHERE date = '").append(queryMap.get(Util.MAP_START_DATE)).append("'");
                sqlQueryCommon.append("AND userid=").append(userid).append(" ");
                sqlQueryCommon.append("AND slot between ").append(queryMap.get(Util.MAP_START_DATE_START_SLOT)).append(" AND ").append(queryMap.get(Util.MAP_END_DATE_END_SLOT));
                sqlQueryCommon.append(" ORDER BY slot ASC;");
                sqlQueryList.add(sqlQueryCommon.toString());
            } else {
                //two queries
                sqlQueryOne = new StringBuilder();
                sqlQueryOne.append("SELECT userid, date, slot, driverstatus, vehiclestatus, distance FROM loggraph WHERE date = '").append(queryMap.get(Util.MAP_START_DATE)).append("'");
                sqlQueryOne.append("AND userid=").append(userid).append(" ");
                sqlQueryOne.append("AND slot between ").append(queryMap.get(Util.MAP_START_DATE_START_SLOT)).append(" AND ").append(queryMap.get(Util.MAP_START_DATE_END_SLOT));
                sqlQueryOne.append(" ORDER BY slot ASC;");
                sqlQueryList.add(sqlQueryOne.toString());


                sqlQueryTwo = new StringBuilder();
                sqlQueryTwo.append("SELECT userid, date, slot, driverstatus,vehiclestatus, distance FROM loggraph WHERE date = '").append(queryMap.get(Util.MAP_END_DATE)).append("'");
                sqlQueryTwo.append("AND userid=").append(userid).append(" ");
                sqlQueryTwo.append("AND slot between ").append(queryMap.get(Util.MAP_END_DATE_START_SLOT)).append(" AND ").append(queryMap.get(Util.MAP_END_DATE_END_SLOT));
                sqlQueryTwo.append(" ORDER BY slot ASC;");
                sqlQueryList.add(sqlQueryTwo.toString());
            }
        } else {
            if (queryMap.get(Util.MAP_START_DATE).equalsIgnoreCase(queryMap.get(Util.MAP_END_DATE))) {
                //one query
                sqlQueryCommon = new StringBuilder();
                sqlQueryCommon.append("SELECT id, userid, date, slot, driverstatus, vehiclestatus, location, calculatedstatus,associatedvehicle, associatedtrailer,annotation, odo, origin, distance FROM loggraph WHERE date = '").append(queryMap.get(Util.MAP_START_DATE)).append("'");
                sqlQueryCommon.append("AND userid=").append(userid).append(" ");
                sqlQueryCommon.append("AND slot between ").append(queryMap.get(Util.MAP_START_DATE_START_SLOT)).append(" AND ").append(queryMap.get(Util.MAP_END_DATE_END_SLOT));
                sqlQueryCommon.append(" ORDER BY slot ASC;");
                sqlQueryList.add(sqlQueryCommon.toString());
            } else {
                //two queries
                sqlQueryOne = new StringBuilder();
                sqlQueryOne.append("SELECT id, userid, date, slot, driverstatus, vehiclestatus, location, calculatedstatus,associatedvehicle, associatedtrailer,annotation, odo, origin, distance FROM loggraph WHERE date = '").append(queryMap.get(Util.MAP_START_DATE)).append("'");
                sqlQueryOne.append("AND userid=").append(userid).append(" ");
                sqlQueryOne.append("AND slot between ").append(queryMap.get(Util.MAP_START_DATE_START_SLOT)).append(" AND ").append(queryMap.get(Util.MAP_START_DATE_END_SLOT));
                sqlQueryOne.append(" ORDER BY slot ASC;");
                sqlQueryList.add(sqlQueryOne.toString());


                sqlQueryTwo = new StringBuilder();
                sqlQueryTwo.append("SELECT id, userid, date, slot, driverstatus, vehiclestatus, location, calculatedstatus,associatedvehicle, associatedtrailer,annotation, odo, origin, distance FROM loggraph WHERE date = '").append(queryMap.get(Util.MAP_END_DATE)).append("'");
                sqlQueryTwo.append("AND userid=").append(userid).append(" ");
                sqlQueryTwo.append("AND slot between ").append(queryMap.get(Util.MAP_END_DATE_START_SLOT)).append(" AND ").append(queryMap.get(Util.MAP_END_DATE_END_SLOT));
                sqlQueryTwo.append(" ORDER BY slot ASC;");
                sqlQueryList.add(sqlQueryTwo.toString());
            }
        }
        return sqlQueryList;
    }

    public static String BitMapToString(Bitmap bitmap){
        ByteArrayOutputStream baos=new  ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG,100, baos);
        byte [] b=baos.toByteArray();
        String temp= Base64.encodeToString(b, Base64.DEFAULT);
        return temp;
    }

    public static Bitmap StringToBitMap(String encodedString){
        try {
            byte [] encodeByte= Base64.decode(encodedString,Base64.DEFAULT);
            Bitmap bitmap= BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
            return bitmap;
        } catch(Exception e) {
            e.getMessage();
            return null;
        }
    }

    public static String getTodayDate(){
        // Setting dates
        Map<String, String> map = UtilDate.getSlotNumberForLoggedInUser();
        return map.get("date");
    }

    public static String getTime(double it) {
        String time = "";
        double hr = it / 4.0;
        String min = Integer.toString((int) (((it / 4.0) % (int) hr) * 60)).equals("0") ? "00" : Integer.toString((int) (((it / 4.0) % (int) hr) * 60));
        String hour = "";
        if (hr < 10.0) {
            hour = "0" + Integer.toString((int) hr);
        } else {
            hour = Integer.toString((int) hr);
        }
        time = hour + ":" + min;
        return time;
    }

    public static String getTimezoneStr(int timezoneid) {
        String value = "";
        switch (timezoneid) {
            case 1:
                value = "PST";
                break;
            case 2:
                value = "PDT";
                break;
            case 3:
                value = "MST";
                break;
            case 4:
                value = "MDT";
                break;
            case 5:
                value = "CST";
                break;
            case 6:
                value = "CDT";
                break;
            case 7:
                value = "EST";
                break;
            case 8:
                value = "EDT";
                break;
            default:
                value = "PST";

        }
        return value;
    }

    public static String getTimezoneGTMStr(int timezoneid) {
        String value = "";
        switch (timezoneid) {
            case 1:
                value = "GMT-8:00";
                break;
            case 2:
                value = "GMT-7:00";
                break;
            case 3:
                value = "GMT-7:00";
                break;
            case 4:
                value = "GMT-6:00";
                break;
            case 5:
                value = "GMT-6:00";
                break;
            case 6:
                value = "GMT-5:00";
                break;
            case 7:
                value = "GMT-5:00";
                break;
            case 8:
                value = "GMT-4:00";
                break;
            default:
                value = "GMT-8:00";
        }
        return value;
    }

    public static String getCycleText(int i) {
        String cyclezone_str = "";
        if (i == 0) {
            cyclezone_str = "USA70hours";
        } else if (i == 1) {
            cyclezone_str = "USA60hours";
        } else if (i == 2) {
            cyclezone_str = "California80hours";
        } else if (i == 3) {
            cyclezone_str = "Texas70hours";
        } else if (i == 4) {
            cyclezone_str = "Other";
        }
        return cyclezone_str;
    }

    public static int getCycleHours(String usercycle) {
        int cyclehours = 70;
        if (usercycle.contains("USA70hours")) {
            cyclehours = 70;
        } else if (usercycle.contains("USA60hours")) {
            cyclehours = 60;
        } else if (usercycle.contains("California80hours")) {
            cyclehours = 80;
        } else if (usercycle.contains("Texas70hours")) {
            cyclehours = 70;
        } else if (usercycle.contains("Other")) {
            cyclehours = 70;
        }
        return cyclehours;
    }

    public static int getCycleDays(String usercycle) {
        int cycledays = 7;
        if (usercycle.contains("USA70hours")) {
            cycledays = 8;
        } else if (usercycle.contains("USA60hours")) {
            cycledays = 7;
        } else if (usercycle.contains("California80hours")) {
            cycledays = 8;
        } else if (usercycle.contains("Texas70hours")) {
            cycledays = 7;
        } else if (usercycle.contains("Other")) {
            cycledays = 7;
        }
        return cycledays;
    }

    public static String getDrivingHours(int countIn15mins) {
        int totalDrivingMins = 0;
        int drivingHours = 0;
        int drivingMins = 0;
        totalDrivingMins = countIn15mins * 15;
        drivingHours = totalDrivingMins / 60;
        drivingMins = totalDrivingMins % 60;
        StringBuilder str = new StringBuilder();
        str.append(String.format("%02d", drivingHours)).append(":").append(String.format("%02d", drivingMins));
        return str.toString();
    }

    public static String getData(String value, String defaultValue) {
        if (defaultValue == null) {
            defaultValue = "";
        }
        if (value != null && !value.equals("")) {
            return value;
        }
        return defaultValue;
    }

    public static String getData(Object value, String defaultValue) {
        if (defaultValue == null) {
            defaultValue = "";
        }
        if (value != null) {
            return value.toString();
        }
        return defaultValue;
    }

    public static int getData(Object value, int defaultValue) {
        if (value != null && value != "") {
            return Integer.valueOf(value.toString());
        }
        return defaultValue;
    }


    public static String getCycle(int cycle_id){
        String cycle = "USA 70 hours/8 days";
        if(cycle_id == 0 || cycle_id == 1){
            cycle = "USA 70 hours/8 days";
        } else if(cycle_id == 2){
            cycle = "USA 60 hours/7 days";
        } else if(cycle_id == 3){
            cycle = "California 80 hours/ 8 days";
        } else if(cycle_id == 4){
            cycle = "Texas 70 hours/7 days";
        } else if(cycle_id == 5){
            cycle = "Other";
        }
        return cycle;
    }

    public static String getTimezoneShort(String timezone){
        String tz = "PST";

        switch(timezone){
            case "Pacific Standard Time (PST)":
                tz = "PST";
                break;

            case "Pacific Daylight Time (PDT)":
                tz = "PDT";
                break;

            case "Mountain Standard Time (MST)":
                tz = "MST";
                break;

            case "Mountain Daylight Time (MDT)":
                tz = "MDT";
                break;

            case "Central Standard Time (CST)":
                tz = "CST";
                break;

            case "Central Daylight Time (CDT)":
                tz = "CDT";
                break;

            case "Eastern Standard Time (EST)":
                tz = "EST";
                break;

            case "Eastern Daylight Time (EDT)":
                tz = "EDT";
                break;

            default:
                tz = "PST";
        }

        return tz;
    }

    // Returns array of dates for past 'n - 1' days and today
    public static ArrayList<String> getDate(int numDays){
        ArrayList<String> dates = new ArrayList<String>(numDays);

        for(int i=0; i<numDays; i++){
            Date today = new Date();
            Calendar cal = new GregorianCalendar();
            cal.setTime(today);
            cal.add(Calendar.DAY_OF_MONTH, -i);
            SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
            dates.add(df.format(cal.getTime()));
        }
        return dates;
    }

    public static String getDeviceIdentiferForSim(){
        StringBuilder str = new StringBuilder();
        Random random = new Random();
        while(str.length() < 15) {
            str.append(random.nextInt(30000));
        }
        return str.toString();
    }

    public static String uiToOverrideStatus(String uiStatus) {
        if (uiStatus != null && !uiStatus.equals("")) {
            if (uiStatus.equalsIgnoreCase("1")) {
                return "5";
            }
            if (uiStatus.equalsIgnoreCase("2")) {
                return "6";
            }
            if (uiStatus.equalsIgnoreCase("3")) {
                return "7";
            }
            if (uiStatus.equalsIgnoreCase("4")) {
                return "8";
            }
        }
        Log.e("status", "Unknown status: " + uiStatus);
        return "";
    }
}
