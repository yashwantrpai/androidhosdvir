package hos.androidhosdvir.util;

import android.util.Log;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.Calendar;
import java.util.*;
import hos.androidhosdvir.db.DBHelper;

public class UtilDate {
    public static String getDateFormated(String dateStr){
        Date dt = new Date();
        SimpleDateFormat df = new SimpleDateFormat("yyyy/M/d");
        try {
            dt = df.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        df.applyPattern("MM/dd/yyyy");
        String date = df.format(dt);
        return date;
    }

    public static int getSlot(String hourStr, String minStr ) {
        int slot = 0;
        if(hourStr != null && !hourStr.equals("") && minStr != null && !minStr.equals("")) {
            int hour = Integer.parseInt(hourStr);
            int min = Integer.parseInt(minStr);
            try {
                slot = hour * 4;
                if (min > 0) {
                    slot = slot + (min / 15);
                    slot = slot + 1;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return slot;
    }

    public static int getSlot(String hourStr, String minStr, String secStr ) {
        int slot = 0;

        if(hourStr != null && !hourStr.equals("") && minStr != null && !minStr.equals("")) {
            int hour = 0;
            int min = 0;
            int sec = 0;

            try{
                hour = Integer.valueOf(hourStr);
            } catch(NumberFormatException ex) {

            }

            try{
                min = Integer.valueOf(minStr);
            } catch(NumberFormatException ex) {

            }

            try{
                sec = Integer.parseInt(secStr);
                if ( sec > 0) {
                    min = min + 1;
                }
            } catch(NumberFormatException ex) {

            }

            int add = 0;

            slot = hour * 4;
            if (min > 0 && min <= 15) {
                add = 1;
            } else if (min > 15 && min <= 30) {
                add = 2;
            } else if (min > 30 && min <= 45) {
                add = 3;
            } else if (min > 45 && min <= 60) {
                add = 4;
            }
            if (min != 0) {
                slot = slot + add;
            }

            if (slot == 0) {
                slot = 1;
            }
        }
        //System.out.println("Slot no:" + slot);

        return slot;
    }

    public static int getSlotForStatus(String hourStr, String minStr ) {
        int slot = 0;

        if(hourStr != null && !hourStr.equals("") && minStr != null && !minStr.equals("")) {
            int hour = Integer.valueOf(hourStr);
            int min = Integer.valueOf(minStr);
            if (hour != 24) {
                min = min - 1;
            }

            int add = 0;

            slot = hour * 4;
            if (min > 0 && min < 15) {
                add = 1;
            } else if (min > 15 && min < 30) {
                add = 2;
            } else if (min > 30 && min < 45) {
                add = 3;
            } else if (min > 45 && min < 60) {
                add = 4;
            }
            if (min != 0) {
                slot = slot + add;
            }

            if (slot == 0) {
                slot = 1;
            }
        }
        Log.i("slot", "Slot no:" + slot);
        return slot;
    }

    public static String getUTCTime(String time, String timediff, boolean start) {
        String result = "";
        Calendar cal = Calendar.getInstance();
        try {
            SimpleDateFormat sourceFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
            //sourceFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            sourceFormat.setTimeZone(TimeZone.getTimeZone(timediff));
            Date parsed = sourceFormat.parse(time);

            cal.setTime(parsed);
            if (start) {

                cal.add(Calendar.MINUTE, 1);

                //date = cal.getTime();
                //cal.getTime()

            } else {

                cal.add(Calendar.MINUTE, -1);
            }

            TimeZone tz = TimeZone.getTimeZone("UTC");
            SimpleDateFormat destFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
            destFormat.setTimeZone(tz);

            result = destFormat.format(cal.getTime());

        } catch(Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }

    public static String getUTCTimeForAddStatus(String time, String timediff, boolean start) {
        String result = "";
        Calendar cal = Calendar.getInstance();
        try {
            SimpleDateFormat sourceFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm");
            //sourceFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            sourceFormat.setTimeZone(TimeZone.getTimeZone(timediff));
            Date parsed = sourceFormat.parse(time);
            cal.setTime(parsed);
            if (start) {

                cal.add(Calendar.MINUTE, 1);

                //date = cal.getTime();
                //cal.getTime()

            } else {

                cal.add(Calendar.MINUTE, -1);
            }

            TimeZone tz = TimeZone.getTimeZone("UTC");
            SimpleDateFormat destFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
            destFormat.setTimeZone(tz);

            result = destFormat.format(cal.getTime());

        } catch(Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }

    public static Map<String, String> getDateHourMin(String str) {
        Map<String, String> dateHourMinMap = new HashMap<>();
        if (str != null) {
            String[] dateTime = str.split("\\ ");
            if (dateTime != null && dateTime.length == 2) {
                dateHourMinMap.put("date", dateTime[0]);
                String[] hourMin = dateTime[1].split("\\:");
                if(hourMin != null && hourMin.length > 1) {
                    dateHourMinMap.put("hour", hourMin[0]);
                    dateHourMinMap.put("min", hourMin[1]);
                    dateHourMinMap.put("sec", hourMin[2]);
                }
            }
        }
        return dateHourMinMap;
    }

    public static Map<String, String> getSlotRangeInUTC(String date, String timediff) {
        String start = getUTCTime(date + " 00:00:00", timediff, true);
        String end = getUTCTime(date + " 23:59:59", timediff, false);
        Map<String, String> startMap = getDateHourMin(start);
        Map<String, String> endMap = getDateHourMin(end);
        Map<String, String> currentMomentMap = getDateHourMin(getCurrentUTCSlotTime());

        String startDateStartSlot = String.valueOf(getSlot(startMap.get("hour"), startMap.get("min") + 1));
        String startDateEndSlot = "96";
        String endDateStartSlot = "1";
        String endDateEndSlot= String.valueOf(getSlot(endMap.get("hour"), endMap.get("min")));
        String currentMomentStartSlot = "1";
        String currentMomentEndSlot = String.valueOf(getSlot(currentMomentMap.get("hour"), currentMomentMap.get("min")));

        Map<String, String> map = new HashMap<>();
        map.put("startDate", startMap.get("date"));
        map.put("startDateStartSlot", startDateStartSlot);
        map.put("startDateEndSlot", startDateEndSlot);
        map.put("endDate", endMap.get("date"));
        map.put("endDateStartSlot", endDateStartSlot);
        map.put("endDateEndSlot", endDateEndSlot);
        map.put("currentMomentDate", currentMomentMap.get("date"));
        map.put("currentMomentStartSlot",currentMomentStartSlot);
        map.put("currentMomentEndSlot", currentMomentEndSlot);
        return map;
    }

    //For Add status
    public static Map<String, String> getSlotRangeInUTCForAddStatus(String startDateTime,String endDateTime, String timediff) {
        //String start = getUTCTime(date + " 00:00:00", timediff);
        //String end = getUTCTime(date + " 23:59:59", timediff);

        //String start = getUTCTime(startDateTime, timediff);
        //String end = getUTCTime(endDateTime, timediff);

        String start = getUTCTimeForAddStatus(startDateTime, timediff, true);
        String end = getUTCTimeForAddStatus(endDateTime, timediff, false);

        Map<String, String> startMap = getDateHourMin(start);
        Map<String, String> endMap = getDateHourMin(end);
        Map<String, String> currentMomentMap = getDateHourMin(getCurrrentUTCTime());

        int startMin = Integer.valueOf(startMap.get("min")) + 1;
        String startDateEndSlot = "";
        String endDateStartSlot = "";
        String endDateEndSlot= "";
        String startDateStartSlot = String.valueOf(getSlotForStatus(startMap.get("hour"), String.valueOf(startMin)));
        if (startMap.get("date").equalsIgnoreCase(endMap.get("date"))) {
            startDateEndSlot = String.valueOf(getSlotForStatus(endMap.get("hour"), endMap.get("min")));
            endDateStartSlot = "1";
            endDateEndSlot = String.valueOf(getSlotForStatus(endMap.get("hour"), endMap.get("min")));
        } else {
            startDateEndSlot = "96";
            endDateStartSlot = "1";
            endDateEndSlot= String.valueOf(getSlotForStatus(endMap.get("hour"), endMap.get("min")));
        }

        String currentMomentStartSlot = "1";
        String currentMomentEndSlot = String.valueOf(getSlotForStatus(currentMomentMap.get("hour"), currentMomentMap.get("min")));

        Map<String, String> map = new HashMap<>();
        map.put("startDate", startMap.get("date"));
        map.put("startDateStartSlot", startDateStartSlot);
        map.put("startDateEndSlot", startDateEndSlot);
        map.put("endDate", endMap.get("date"));
        map.put("endDateStartSlot", endDateStartSlot);
        map.put("endDateEndSlot", endDateEndSlot);
        map.put("currentMomentDate", currentMomentMap.get("date"));
        map.put("currentMomentStartSlot",currentMomentStartSlot);
        map.put("currentMomentEndSlot", currentMomentEndSlot);
        return map;
    }

    public static String getCurrrentUTCTime(){
        SimpleDateFormat sourceFormatUTC = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        SimpleDateFormat destFormatUTC = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        sourceFormatUTC.setTimeZone(TimeZone.getTimeZone("UTC"));
        String dateString = sourceFormatUTC.format(new Date());
        return dateString;
    }

    public static String getCurrentUTCSlotTime(){
        String result = "";
        Calendar cal = Calendar.getInstance();

        try {
            SimpleDateFormat sourceFormatUTC = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
            SimpleDateFormat destFormatUTC = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
            sourceFormatUTC.setTimeZone(TimeZone.getTimeZone("UTC"));
            String dateString = sourceFormatUTC.format(new Date());
            Date parsed = sourceFormatUTC.parse(dateString);
            cal.setTime(parsed);
            //cal.add(Calendar.MINUTE, -15);
            TimeZone tz = TimeZone.getTimeZone("UTC");
            SimpleDateFormat destFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
            destFormat.setTimeZone(tz);
            result = destFormat.format(cal.getTime());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }

    public static Date getCurrrentLocalDateInDateObject(String timeDiff){
        SimpleDateFormat sourceFormatUTC = new SimpleDateFormat("MM/dd/yyyy");
        //SimpleDateFormat destFormatUTC = new SimpleDateFormat("MM/dd/yyyy");
        //SimpleDateFormat destFormatUTC = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        //sourceFormatUTC.setTimeZone(TimeZone.getTimeZone("GMT-8:00"));
        sourceFormatUTC.setTimeZone(TimeZone.getTimeZone(timeDiff));
        Date parsedDate = null;
        //String dateString = "";
        /*try{
            Date parsedDate = sourceFormatUTC.parse(getCurrrentUTCTime());
            dateString = destFormatUTC.format(parsedDate);
        }catch(Exception ex){
            Log.e("parse", "Error:", ex);
        }*/

        String dateString = sourceFormatUTC.format(new Date());
        try
        {
            parsedDate = sourceFormatUTC.parse(dateString);
        }catch(Exception ex){
            Log.e("error", "Error ", ex);
        }

        return parsedDate;
    }

    public static String getCurrrentLocalDate(String timeDiff){
        SimpleDateFormat sourceFormatUTC = new SimpleDateFormat("MM/dd/yyyy");
        //SimpleDateFormat destFormatUTC = new SimpleDateFormat("MM/dd/yyyy");
        //SimpleDateFormat destFormatUTC = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        //sourceFormatUTC.setTimeZone(TimeZone.getTimeZone("GMT-8:00"));
        sourceFormatUTC.setTimeZone(TimeZone.getTimeZone(timeDiff));

        //String dateString = "";
        /*try{
            Date parsedDate = sourceFormatUTC.parse(getCurrrentUTCTime());
            dateString = destFormatUTC.format(parsedDate);
        }catch(Exception ex){
            Log.e("parse", "Error:", ex);
        }*/

        String dateString = sourceFormatUTC.format(new Date());
        return dateString;
    }

    public static String getCurrrentLocalDateTime(String timeDiff){
        SimpleDateFormat sourceFormatUTC = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        //SimpleDateFormat sourceFormatUTC = new SimpleDateFormat("MM/dd/yyyy HH:mm");
        //SimpleDateFormat destFormatUTC = new SimpleDateFormat("MM/dd/yyyy HH:mm");
        //SimpleDateFormat destFormatUTC = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        //sourceFormatUTC.setTimeZone(TimeZone.getTimeZone("GMT-8:00"));
        sourceFormatUTC.setTimeZone(TimeZone.getTimeZone(timeDiff));

        /*String dateString = "";
        try{
            Date parsedDate = sourceFormatUTC.parse(getCurrrentUTCTime());
            dateString = destFormatUTC.format(parsedDate);
        }catch(Exception ex){
            Log.e("parse", "Error:", ex);
        }*/

        String dateString = sourceFormatUTC.format(new Date());
        return dateString;
    }

    /*public static String getCurrrentLocalDateTime(String userDateTime, String timeDiff){
        SimpleDateFormat sourceFormatUTC = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        SimpleDateFormat destFormatUTC = new SimpleDateFormat("MM/dd/yyyy HH:mm");
        //SimpleDateFormat destFormatUTC = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        //sourceFormatUTC.setTimeZone(TimeZone.getTimeZone("GMT-8:00"));
        sourceFormatUTC.setTimeZone(TimeZone.getTimeZone(timeDiff));
        String dateString = destFormatUTC.format(userDateTime);
        return dateString;
    }*/

    public static Map<String, String> getSlotRangeInUTCFromPST(String date, String timeDiff) {
        return getSlotRangeInUTC(date, timeDiff);
    }

    public static Map<String, String> getSlotNumberForLoggedInUser() {
        String dateTime = getCurrrentLocalDateTime(DBHelper.getLoggedInUserDefaultTimezoneInGTM());
        Map<String, String> map = getDateHourMin(dateTime);
        if(map != null) {
            String slot = String.valueOf(getSlot(map.get("hour"), map.get("min")));
            map.put("slot", slot);
        }
        return map;
    }

    public static Map<String, String> getCurrrentLocalDateTimeInPST() {
        String dateTime = getCurrrentLocalDateTime("GMT-8:00");
        Map<String, String> map = getDateHourMin(dateTime);
        if(map != null) {
            String slot = String.valueOf(getSlot(map.get("hour"), map.get("min")));
            map.put("slot", slot);
        }
        return map;
    }

    public static Map<String, String>  getCurrrentLocalDateTimeInPDT() {
        String dateTime = getCurrrentLocalDateTime("GMT-7:00");
        Map<String, String> map = getDateHourMin(dateTime);
        if(map != null) {
            String slot = String.valueOf(getSlot(map.get("hour"), map.get("min")));
            map.put("slot", slot);
        }
        return map;
    }

    /*public static boolean isStartAndEndLessThanCurrentUTC(String start, String end){
        String loggedUserTimezone = DBHelper.getLoggedInUserDefaultTimezoneInGTM();
        String currentUTCStr = getCurrrentUTCTime();
        String currentLocalStr = getCurrrentLocalDateTime(loggedUserTimezone);
        String localDate = "";
        String constructedStart = "";
        String constructedEnd = "";
        String startUTCStr = "";
        String endUTCStr = "";

        try{
            if (currentLocalStr != null) {
                String[] values = currentLocalStr.split(" ");
                if (values != null && values.length == 2) {
                    localDate = values[0].trim();
                }
            }


            constructedStart = localDate + " " + start;
            constructedEnd = localDate + " " + end;
            startUTCStr = getCurrrentLocalDateTime(constructedStart,loggedUserTimezone);
            endUTCStr = getCurrrentLocalDateTime(constructedEnd,loggedUserTimezone);

        } catch(Exception ex) {
            Log.e("date", "Error:", ex);
        }


        TimeZone utcTimezone = TimeZone.getTimeZone("UTC");

        SimpleDateFormat currentMomentFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        SimpleDateFormat startFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm");
        SimpleDateFormat endFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm");

        currentMomentFormat.setTimeZone(utcTimezone);
        startFormat.setTimeZone(utcTimezone);
        endFormat.setTimeZone(utcTimezone);

        Calendar calCurrentMoment = Calendar.getInstance();
        Calendar calStart = Calendar.getInstance();
        Calendar calEnd = Calendar.getInstance();

        calCurrentMoment.setTimeZone(utcTimezone);
        calStart.setTimeZone(utcTimezone);
        calEnd.setTimeZone(utcTimezone);

        Date currentMomentParsed = null;
        Date startParsed = null;
        Date endParsed = null;

        try {
            currentMomentParsed = currentMomentFormat.parse(currentUTCStr);
            startParsed = currentMomentFormat.parse( startUTCStr);
            endParsed = currentMomentFormat.parse( endUTCStr);
        } catch(Exception ex) {
            Log.e("date", "Error:", ex);
        }



        calCurrentMoment.setTime(currentMomentParsed);



    }*/

    public static int getDelayValueForThread(){
        Calendar c = Calendar.getInstance();
        int min = c.get(Calendar.MINUTE);
        int delay =  (15 - (min % 15)) * 1000 * 60;
        return delay;
    }

    public static String getTodaysDate() {
        String timediff = DBHelper.getLoggedInUserDefaultTimezoneInGTM();
        String result = "";
        try{
            TimeZone tz = TimeZone.getTimeZone(timediff);
            SimpleDateFormat sourceFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
            sourceFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            sourceFormat.setTimeZone(tz);
            Date parsed = sourceFormat.parse(getCurrrentUTCTime());

            String temp = parsed.toString();

            //Calendar cal = Calendar.getInstance(tz);
            Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
            cal.setTimeZone(TimeZone.getTimeZone(timediff));
            cal.setTime(parsed);

            SimpleDateFormat destFormat = new SimpleDateFormat("MM/dd/yyyy");
            destFormat.setTimeZone(tz);
            result = destFormat.format(cal.getTime());

        } catch(Exception ex){
            Log.e("today", "Today's date" , ex);
        }
       return result;
    }

    public static long timeDiffInSeconds(String dateStart, String dateStop){

        //HH converts hour in 24 hours format (0-23), day calculation
        SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        long diffMins = 0;
        Date d1 = null;
        Date d2 = null;

        try {
            d1 = format.parse(dateStart);
            d2 = format.parse(dateStop);

            //in milliseconds
            long diff = d2.getTime() - d1.getTime();

           diffMins = (diff / 1000)/60;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return  diffMins;
    }

    public static String getEndOfSlotTime(String datetime){
        String date = "";
        String hour = "";
        String min = "";
        String sec = "";
        int minInt = 0;
        int minCorrected = 0;

        if (datetime != null) {
            String[] values = datetime.split(" ");
            if (values != null && values.length == 2) {
                if (values[0] != null ) {
                    date = values[0].trim();
                }
                if (values[1] != null) {
                    String[] times = values[1].trim().split(":");
                    if(times != null && times.length == 3) {
                        hour = times[0];
                        min = times[1];
                        sec = times[2];
                    }
                }
            }
        }
        try{
            minInt= Integer.parseInt(min);
            if(minInt > 45) {
                minCorrected = 59;
            } else if (minInt > 30) {
                minCorrected = 44;
            } else if (minInt > 15) {
                minCorrected = 29;
            } else {
                minCorrected = 14;
            }
        }catch (NumberFormatException ex){
            Log.e("endslot", "End slot end time", ex);
        }
        StringBuilder correctedDate = new StringBuilder();
        correctedDate.append(date);
        correctedDate.append(" ");
        correctedDate.append(hour).append(":").append(minCorrected).append(":").append("59");
        return correctedDate.toString();
    }
}
